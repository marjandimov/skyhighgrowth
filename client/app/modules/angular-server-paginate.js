﻿angular.module('ngServerPaginate', [])
    .directive('paginator', ['$rootScope', function ($rootScope) {
    return {
        restrict: 'E',
        scope: {
            skip: '=',
            limit: '=',
            count: '=',
            pages: '=',
            callback: '&'
        },
        controller: function ($rootScope, $scope) {
            $scope.currentPage = 1;
            $scope.next = function () {
                $scope.currentPage++;
                $scope.skip = parseInt($scope.skip) + parseInt($scope.limit);
                $scope.callback({ arg1: $scope.skip, arg2: 'next'});
            };
            $scope.prev = function () {
                $scope.currentPage--;
                $scope.skip = parseInt($scope.skip) - parseInt($scope.limit);
                if ($scope.skip  < 0) {
                    $scope.skip = 0;
                }
                $scope.callback({ arg1: $scope.skip , arg2: 'prev' });
            };
            $scope.last = function () {
                $scope.currentPage = $scope.pages
                $scope.skip = parseInt($scope.pages);
                $scope.callback({ arg1: $scope.skip, arg2: 'last' });
            };
            $scope.first = function () {
                $scope.currentPage = 1;
                $scope.skip = 0;
                $scope.callback({ arg1: $scope.skip, arg2: 'first' });
            };
        },
        link: function (scope, element, attrs, Paginator, ngModel) {
            
        },
        templateUrl: 'app/modules/templates/pagination.html'
    };
}]);
