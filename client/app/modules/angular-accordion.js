﻿angular.module('ngAccordion', [])
    .directive('accordion', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {

            },
            templateUrl: 'app/modules/templates/accordion.html',
            controller: function ($rootScope, $scope) {
                $scope.id = $rootScope.generateGUID();
            },
            link: function (scope, element, attrs, Paginator, ngModel) {
            
            }
        };
    }])
    .directive('accordionHeader', ['$rootScope','$document', function ($rootScope, $document) {
        return {
            restrict: 'E',
            require: ['^accordion'],
            transclude: true,
            replace: true,
            scope: true,
            templateUrl: 'app/modules/templates/accordion-header.html',
            controller: function ($rootScope, $scope) {
                $scope.headerClick = function ($event) {
                    $($event.currentTarget).next('.accordion-body').slideToggle();
                    $($event.currentTarget).parent().toggleClass('opened');
                };
            },
            link: function (scope, element, attrs) {

            }
        };
    }])
    .directive('accordionBody', ['$rootScope', '$document', function ($rootScope, $document) {
        return {
            restrict: 'E',
            require: ['^accordion'],
            transclude: true,
            replace: true,
            scope: true,
            templateUrl: 'app/modules/templates/accordion-body.html',
            controller: function ($rootScope, $scope) {

            },
            link: function (scope, element, attrs) {

            }
        };
    }])

