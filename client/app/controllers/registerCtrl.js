﻿shgApp.controller('registerCtrl', ['$rootScope', '$scope', '$cookies', 'dataService', function ($rootScope, $scope, $cookies, dataService) {
    $rootScope.title = 'Welcome to Shy High Growth';
    $rootScope.description = 'We bring you clients';
    $rootScope.keywords = 'csv,db,shg';

    $scope.genders = [
        { name: 'Male', value: 'Male' },
        { name: 'Female', value: 'Female' }
    ];

    $scope.register = function () {
        if (!$scope.shgform.$invalid) {
            dataService.register().exec({}, $scope.obj).$promise.then(function (res, err) {
                if (res.status.code === 200) {
                    var loginuser = {
                        username: $scope.obj.username,
                        password: $scope.obj.password
                    };
                    dataService.login().exec({}, loginuser).$promise.then(function (res, err) {
                        console.log(res.status);
                        if (res.status.code === 200) {
                            $rootScope.loggedUser = res.data;
                            $cookies.put('shgauth', res.token);
                            
                            $rootScope.changeView('/admin/dashboard');
                        }
                    });
                }
            });
        }
    };

}]);
