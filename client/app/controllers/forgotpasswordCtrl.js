﻿shgApp.controller('forgotpasswordCtrl', ['$rootScope', '$scope', 'dataService', function ($rootScope, $scope, dataService) {
    $rootScope.title = 'Welcome to Shy High Growth';
    $rootScope.description = 'We bring you clients';
    $rootScope.keywords = 'csv,db,shg';

    $scope.forgotpassword = function () {
        if (!$scope.shgform.$invalid) {
            dataService.forgotpassword().exec({}, $scope.data).$promise.then(function (res, err) {
                
            });
        }
    };

}]);
