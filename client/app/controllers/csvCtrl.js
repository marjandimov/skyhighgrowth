﻿shgApp.controller('csvCtrl', ['$rootScope', '$scope', 'dataService', function ($rootScope, $scope, dataService) {
    $rootScope.title = 'Welcome to Shy High Growth';
    $rootScope.description = 'We bring you clients';
    $rootScope.keywords = 'csv,db,shg';

    dataService.getAllLists(function (res) {
        $scope.groups = res.data;
        
    });

    $scope.downloadSample = function (listId) {
        console.log(listId);
        dataService.downloadSample({id: listId}, function (res) {

        });
    };

}]);
