﻿shgApp.controller('dbCtrl', ['$rootScope', '$scope', '$cookies', 'dataService', 'flashService', 'Upload', function ($rootScope, $scope, $cookies, dataService, flashService, Upload) {

    $scope.selected = [];

    $scope.searchType = 'Companies';
    $scope.obj = {};
    $scope.data = [];
    $scope.ctrl = 'db';
    $scope.searchObj = {};

    $scope.industries = [];
    $scope.locations = [];
    $scope.industryTags = [];
    $scope.countries = [];
    $scope.specialities = [];
    $scope.numberOfEmployees = [];

    $scope.skip = 0;
    $scope.limit = 10;
    $scope.count = 0;
    $scope.pages = 0;

    $scope.reset = function () {
        $scope.result = void (0);
    };

    $scope.add = function (obj) {
        if (!$rootScope.lodashFindBy($scope.selected, '_id', obj._id)) {
            $scope.selected.push(obj);
        }
    };
    $scope.remove = function (obj) {
        _.remove($scope.selected, function (currentObject) {
            return obj._id === obj._id;
        });
    };

    $scope.checkOne = function (checked, obj) {
        if (checked) {
            $scope.add(obj);
        }
        else {
            $scope.remove(obj);
        }
    };

    $scope.exportSelected = function () {
        var ids = _.map($scope.selected, function (item) { return { '_id': item._id }; });
        dataService.exportSelectedDb({
            'ExportType': $scope.searchType,
            'ids': ids
        },
        function (res) {
            $scope.selected = [];
            $scope.count = 0;
            $scope.pages = 0;
            $scope.result = void (0);
        });
    };

    $scope.getAllFilters = function () {
        dataService.filters('db', 'filters', function (res) {
            $scope.industries = $rootScope.getAllEntities(res.data.industries);
            $scope.locations = $rootScope.getAllEntities(res.data.locations);
            $scope.industryTags = $rootScope.getAllEntities(res.data.industryTags);
            $scope.countries = $rootScope.getAllEntities(res.data.countries);
            $scope.specialities = $rootScope.getAllEntities(res.data.specialities);
            $scope.numberOfEmployees = $rootScope.getAllEntities(res.data.numberOfEmployees);
        });
    };

    $scope.cb = function (skip, cmd) {
        $scope.skip = skip;
        $scope.search();
    };
    $scope.search = function () {
        dataService.search('db', 'search', $scope.skip, $scope.limit, {
            'SearchType': $scope.searchType,
            'Title': $scope.searchObj.Title,
            'FirstName': $scope.searchObj.FirstName,
            'LastName': $scope.searchObj.LastName,
            'Email': $scope.searchObj.Email,
            'Gender': $scope.searchObj.Gender,
            'CompanyName': $scope.searchObj.CompanyName,
            'Industry': $scope.searchObj.Industry,
            'IndustryTag': $scope.searchObj.IndustryTag,
            'Country': $scope.searchObj.Country,
            'Specialities': $scope.searchObj.Specialities,
            'NoOfEmployees': $scope.searchObj.NoOfEmployees,
            'Products': $scope.searchObj.Products,
            'Technologies': $scope.searchObj.Technologies,
            'InvestorType': $scope.searchObj.InvestorType,
            'Location': $scope.searchObj.Location
        },
            function (res) {
                $scope.reset();
                $scope.result = res.data;
                $scope.count = res.status.count;
                $scope.pages = res.status.pages;
            });
    };

    $scope.getAllFilters();
}]);