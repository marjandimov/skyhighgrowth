﻿shgApp.controller('fileCtrl', ['$rootScope', '$scope', '$cookies', 'dataService', 'Upload', function ($rootScope, $scope, $cookies, dataService, Upload) {

    $scope.aoe = false;
    $scope.isEdit = false;
    $scope.obj = {};

    $scope.getAll = function () {
        dataService.getAll('file', function (res) {
            $scope.files = res.data;
        });
    };

    $scope.cancel = function () {
        $scope.aoe = false;
        $scope.getAll();
    };

    $scope.select = function (obj) {
        $scope.obj = obj;
        $scope.aoe = true;
    };

    $scope.delete = function (name) {
        dataService.delete('file', name, function (res) {
            $scope.aoe = false;
            $scope.getAll();
        });
    };

    $scope.upload = function (file) {
        if ($scope.file) {
            return;
        }
        Upload.upload({
            url: $rootScope.getUrl() + '/api/file',
            headers: {
                'Authorization': 'Bearer ' + $cookies.get('shgauth')
            },
            data: { file: file }
        }).then(function (res) {
            $scope.aoe = false;
            $scope.obj = {};
            $scope.getAll();

        }, function (res) {
            if (res.data.status) {

            }
        }, function (evt) {

        });
    };      

    $scope.getAll();
}]);