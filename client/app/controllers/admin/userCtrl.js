﻿shgApp.controller('userCtrl', ['$rootScope', '$scope', 'dataService', function ($rootScope, $scope, dataService) {
    $scope.aoe = false;
    $scope.isEdit = false;
    $scope.obj = {};
    $scope.roles = {};
    $scope.genders = [
        { name: 'Male', value: 'Male' },
        { name: 'Female', value: 'Female' }
    ];

    $scope.getAll = function () {
        dataService.getAll('role', function (res) {
            $scope.roles = res.data;
            dataService.getAll('user', function (res) {
                $scope.users = res.data;
            });
        });
       
    };

    $scope.cancel = function () {
        $scope.aoe = false;
        $scope.getAll();
    };

    $scope.select = function (obj) {
        $scope.obj = obj;
        $scope.aoe = true;
        $scope.getAll();
    };

    $scope.delete = function (id) {
        dataService.delete('user', id, function (res) {
            $scope.aoe = false;
            $scope.getAll();
        });
    };

    $scope.addOrEdit = function () {
        if ($scope.obj._id) {
            dataService.update('user', $scope.obj, function (res) {
                $scope.aoe = false;
                $scope.getAll();
            });
        }
        else {
            dataService.create('user', $scope.obj, function (res) {
                $scope.aoe = false;
                $scope.getAll();
            });
        }
    };

    $scope.getAll();
}]);
