﻿shgApp.controller('iesCtrl', ['$rootScope', '$scope', '$cookies', '$timeout', 'dataService', 'flashService', 'Upload', function ($rootScope, $scope, $cookies, $timeout, dataService, flashService, Upload) {

    $scope.showExport = false;
    $scope.exportAll = false;

    $scope.selected = [];

    $scope.importType = 'Companies';
    $scope.obj = {};
    $scope.data = [];
    $scope.ctrl = 'ies';
    $scope.searchObj = {};

    $scope.industries = [];
    $scope.locations = [];
    $scope.industryTags = [];
    $scope.countries = [];
    $scope.specialities = [];
    $scope.numberOfEmployees = [];

    $scope.skip = 0;
    $scope.limit = 10;
    $scope.count = 0;
    $scope.pages = 0;

    $scope.reset = function () {
        $scope.showExport = false;
        $scope.exportAll = false;
        $scope.result = void (0);
    };

    $scope.exportAllOnChange = function (checked) {
        console.log(checked)
        if (checked) {
            $scope.showExport = true;
        }
        else {
            $scope.showExport = false;
        }
    };
    
    $scope.exportSelected = function () {
        var ids = _.map($scope.selected, function (item) { return { '_id': item._id }; });
        dataService.exportSelectedIes({
            'ExportType': $scope.importType,
            'ids': ids
        },
            function (res) {
                $scope.selected = [];
                $scope.count = 0;
                $scope.pages = 0;
                $scope.result = void (0);
            });
    };

    $scope.add = function (obj) {
        if (!$rootScope.lodashFindBy($scope.selected, '_id', obj._id)) {
            $scope.selected.push(obj);
        }
    };
    $scope.remove = function (obj) {
        _.remove($scope.selected, function (currentObject) {
            return obj._id === obj._id;
        });
    };

    $scope.checkOne = function (checked, obj) {
        if (checked) {
            $scope.add(obj);
        }
        else {
            $scope.remove(obj);
        }
        console.log($scope.selected);
    };

    $scope.checkAll = function (check) {
        if (check) {
            _.forEach($scope.result, function (item) {
                $scope.selected.push(item._id);
            });
        }
        else {
            _.forEach($scope.result, function (item) {
                $scope.selected.push(item._id);
            });
        }
        console.log($scope.selected.length);
    };

    $scope.getChkModel = function (id) {
        if ($scope.selected.indexOf(id) === -1) {
            return false;
        }
        else {
            return true;
        }
    };

    $scope.getAllFilters = function () {
        dataService.filters('ies', 'filters', function (res) {
            $scope.industries = $rootScope.getAllEntities(res.data.industries);
            $scope.locations = $rootScope.getAllEntities(res.data.locations);
            $scope.industryTags = $rootScope.getAllEntities(res.data.industryTags);
            $scope.countries = $rootScope.getAllEntities(res.data.countries);
            $scope.specialities = $rootScope.getAllEntities(res.data.specialities);
            $scope.numberOfEmployees = $rootScope.getAllEntities(res.data.numberOfEmployees);
        });
    };

    $scope.cb = function (skip, cmd) {
        $scope.skip = skip;
        $scope.search();
    };

    $scope.exportAllSearch = function () {
        dataService.exportAllSearch({
            'SearchType': $scope.importType,
            'Title': $scope.searchObj.Title,
            'FirstName': $scope.searchObj.FirstName,
            'LastName': $scope.searchObj.LastName,
            'Email': $scope.searchObj.Email,
            'Gender': $scope.searchObj.Gender,
            'CompanyName': $scope.searchObj.CompanyName,
            'Industry': $scope.searchObj.Industry,
            'IndustryTag': $scope.searchObj.IndustryTag,
            'Country': $scope.searchObj.Country,
            'Specialities': $scope.searchObj.Specialities,
            'NoOfEmployees': $scope.searchObj.NoOfEmployees,
            'Products': $scope.searchObj.Products,
            'Technologies': $scope.searchObj.Technologies,
            'InvestorType': $scope.searchObj.InvestorType,
            'Location': $scope.searchObj.Location
        },
            function (res) {

            });
    };

    $scope.search = function () {
        dataService.search('ies', 'search', $scope.skip, $scope.limit, {
            'SearchType': $scope.importType,
            'Title': $scope.searchObj.Title,
            'FirstName': $scope.searchObj.FirstName,
            'LastName': $scope.searchObj.LastName,
            'Email': $scope.searchObj.Email,
            'Gender': $scope.searchObj.Gender,
            'CompanyName': $scope.searchObj.CompanyName,
            'Industry': $scope.searchObj.Industry,
            'IndustryTag': $scope.searchObj.IndustryTag,
            'Country': $scope.searchObj.Country,
            'Specialities': $scope.searchObj.Specialities,
            'NoOfEmployees': $scope.searchObj.NoOfEmployees,
            'Products': $scope.searchObj.Products,
            'Technologies': $scope.searchObj.Technologies,
            'InvestorType': $scope.searchObj.InvestorType,
            'Location': $scope.searchObj.Location
        },
            function (res) {
                $scope.reset();
                $scope.result = res.data;
                $scope.count = res.status.count;
                $scope.pages = res.status.pages;
            });
    };

    $scope.clear = function () {
        $("input[type='file']").val(null);
    };
   
    $scope.upload = function ($file, importType) {
        if (!$file) {
            return;
        }
        $rootScope.$broadcast('loading', { show: true });
        Upload.upload({
            url: $rootScope.getUrl() + '/api/ies/import',
            headers: {
                'Authorization': 'Bearer ' + $cookies.get('shgauth')
            },
            data: { file: $file, 'importType': importType }
        }).then(function (res) {
            console.log('import doneeee!')
            $rootScope.$broadcast('loading', { show: false });
            flashService.ok(res.data.status);

            $file = null;
            
        }, function (res) {
            if (res.data.status) {
                $timeout(function () {
                    $scope.file = undefined;
                    //workaround for bug in ngFileUpload!
                    var x = $("input:file");
                    var i;
                    for (i = 0; i < x.length; i++) {
                        x[i].value = null;
                    }
                });
                $rootScope.$broadcast('loading', { show: false });
                flashService.err(res.data.status);
            }
        }, function (evt) {

        });
    };

    $scope.getAllFilters();
}]);