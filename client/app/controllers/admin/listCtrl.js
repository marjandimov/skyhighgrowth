﻿shgApp.controller('listCtrl', ['$rootScope', '$scope', '$q', 'dataService', function ($rootScope, $scope, $q, dataService) {

    $scope.tinymceOptions = {
        onChange: function (e) {
            // put logic here for keypress and cut/paste changes
        },
        inline: false,
        plugins: 'advlist autolink link image lists charmap print preview',
        skin: 'lightgray',
        theme: 'modern'
    };

   
    $scope.aoe = false;
    $scope.isEdit = false;
    $scope.obj = {};

    $scope.getAll = function () {
        $q.all([
            dataService.getAll('list'),
            dataService.getAll('group'),
            dataService.getAll('file')
        ])
        .then(function (res) {
            $scope.lists = res[0].data;
            $scope.groups = res[1].data;
            $scope.files = res[2].data;
        })
        .catch(function (data) {

        })
        .finally(function () {

        });
    };

    $scope.cancel = function () {
        $scope.aoe = false;
        $scope.getAll();
    };

    $scope.select = function (obj) {
        $scope.obj = obj;
        $scope.aoe = true;
        $scope.getAll();
    };

    $scope.delete = function (id) {
        dataService.delete('list', id, function (res) {
            $scope.aoe = false;
            $scope.getAll();
        });
    };

    $scope.addOrEdit = function () {

        console.log($scope.obj);
        console.log($scope.selectedGroup);

        //return;

        if ($scope.obj._id) {
            dataService.update('list', $scope.obj, function (res) {
                $scope.aoe = false;
                $scope.getAll();
            });
        }
        else {
            dataService.create('list', $scope.obj, function (res) {
                $scope.aoe = false;
                $scope.getAll();
            });
        }
    };

    $scope.getAll();
}]);