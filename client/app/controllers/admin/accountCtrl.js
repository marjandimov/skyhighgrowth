﻿shgApp.controller('accountCtrl', ['$rootScope', '$scope', 'dataService', function ($rootScope, $scope, dataService) {

    $scope.aoe = false;
    $scope.isEdit = false;
    $scope.obj = {};
    $scope.roles = {};
    $scope.genders = [
        { name: 'Male', value: 'Male' },
        { name: 'Female', value: 'Female' }
    ];

    $scope.getAll = function () {
        dataService.getOwnAccount('user', $rootScope.loggedUser._id, function (res) {
            $scope.obj = res.data;
        });
    };

    $scope.cancel = function () {
        $scope.aoe = false;
        $scope.getAll();
    };

    $scope.select = function (obj) {
        $scope.obj = obj;
        $scope.aoe = true;
        console.log($scope.aoe);
        console.log($scope.obj);
    };
    

    $scope.addOrEdit = function () {
        if ($scope.obj._id) {
            dataService.updateOwnAccount('user', $scope.obj, function (res) {
                $scope.aoe = false;
                $scope.getAll();
            });
        }
    };

    $scope.getAll();
}]);

