﻿shgApp.controller('companyCtrl', ['$rootScope', '$scope', 'dataService', function ($rootScope, $scope, dataService) {

    $scope.aoe = false;
    $scope.isEdit = false;
    $scope.obj = {};
    $scope.data = [];
    $scope.ctrl = 'company';

    //$scope.getAll = function () {
    //    dataService.getAll('company', 0, 10, function (res) {
    //        $scope.data = res.data;
    //    });
    //};

    $scope.cancel = function () {
        $scope.aoe = false;
        $scope.getAll();
    };

    $scope.select = function (obj) {
        $scope.obj = obj;
        $scope.aoe = true;
        $scope.getAll();
    };

    $scope.delete = function (id) {
        dataService.delete('company', id, function (res) {
            $scope.aoe = false;
            $scope.getAll();
        });
    };

    $scope.addOrEdit = function () {
        if ($scope.obj._id) {
            dataService.update('company', $scope.obj, function (res) {
                $scope.aoe = false;
                $scope.getAll();
            });
        }
        else {
            dataService.create('company', $scope.obj, function (res) {
                $scope.aoe = false;
                $scope.getAll();
            });
        }
    };
    $scope.upload = function (file) {
        if ($scope.file) {
            return;
        }
        Upload.upload({
            url: $rootScope.getUrl() + '/company/uploadcsv',
            data: { file: file }
        }).then(function (res) {
            $timeout(function () {
                $scope.file = [];
            });

            if (res.data) {
                $timeout(function () {
                    $scope.file = undefined;
                    //workaround for bug in ngFileUpload!
                    var x = $("input:file");
                    var i;
                    for (i = 0; i < x.length; i++) {
                        x[i].value = null;
                    }
                });
            }

        }, function (res) {
            if (res.data.status) {
                $timeout(function () {
                    $scope.file = undefined;
                    //workaround for bug in ngFileUpload!
                    var x = $("input:file");
                    var i;
                    for (i = 0; i < x.length; i++) {
                        x[i].value = null;
                    }
                });
                flashService.err(res.data.status);
            }
        }, function (evt) {

        });
    };

    //$scope.getAll();
}]);