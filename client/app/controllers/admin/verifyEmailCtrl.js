﻿shgApp.controller('verifyEmailCtrl', ['$rootScope', '$scope', '$cookies', '$timeout', 'dataService', 'flashService', 'Upload', function ($rootScope, $scope, $cookies, $timeout, dataService, flashService, Upload) {
    
   
    $scope.upload = function ($file, importType) {
        if (!$file) {
            return;
        }
        $rootScope.$broadcast('loading', { show: true });
        Upload.upload({
            url: $rootScope.getUrl() + '/api/email/verify',
            headers: {
                'Authorization': 'Bearer ' + $cookies.get('shgauth')
            },
            data: { file: $file, 'importType': importType }
        }).then(function (res) {
            console.log('import doneeee!')
            $rootScope.$broadcast('loading', { show: false });
            flashService.ok(res.data.status);

            $file = null;
            
        }, function (res) {
            if (res.data.status) {
                $timeout(function () {
                    $scope.file = undefined;
                    //workaround for bug in ngFileUpload!
                    var x = $("input:file");
                    var i;
                    for (i = 0; i < x.length; i++) {
                        x[i].value = null;
                    }
                });
                $rootScope.$broadcast('loading', { show: false });
                flashService.err(res.data.status);
            }
        }, function (evt) {

        });
    };

    $scope.export = function () {
        //exportVerifiedEmails

        dataService.exportVerifiedEmails(function (res) {
                
        });
    };
}]);