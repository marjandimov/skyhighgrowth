﻿shgApp.controller('groupCtrl', ['$rootScope', '$scope', 'dataService', function ($rootScope, $scope, dataService) {

    $scope.aoe = false;
    $scope.isEdit = false;
    $scope.obj = {};

    $scope.getAll = function () {
        dataService.getAll('group', function (res) {
            $scope.groups = res.data;
        });
    };

    $scope.cancel = function () {
        $scope.aoe = false;
        $scope.getAll();
    };

    $scope.select = function (obj) {
        $scope.obj = obj;
        $scope.aoe = true;
        $scope.getAll();
    };

    $scope.delete = function (id) {
        dataService.delete('group', id, function (res) {
            $scope.aoe = false;
            $scope.getAll();
        });
    };

    $scope.addOrEdit = function () {
        if ($scope.obj._id) {
            dataService.update('group', $scope.obj, function (res) {
                $scope.aoe = false;
                $scope.getAll();
            });
        }
        else {
            dataService.create('group', $scope.obj, function (res) {
                $scope.aoe = false;
                $scope.getAll();
            });
        }
    };

    $scope.getAll();
}]);