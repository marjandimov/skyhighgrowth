﻿shgApp.controller('homeCtrl', ['$rootScope', '$scope', '$cookies', 'dataService', function ($rootScope, $scope, $cookies, dataService) {
    $rootScope.title = 'Welcome to Shy High Growth';
    $rootScope.description = 'We bring you clients';
    $rootScope.keywords = 'csv,db,shg';

    $scope.contactus = function (obj) {
        if (!$scope.shgform.$invalid) {
            dataService.contactus(obj, function (res) { });
        }
    };

    if ($cookies.get('shgauth')) {
        dataService.getLoggedUser(function (res) {
            $rootScope.loggedUser = res.data;
        });
    }

}]);
