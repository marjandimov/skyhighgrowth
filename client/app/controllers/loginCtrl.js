﻿shgApp.controller('loginCtrl', ['$rootScope', '$scope', '$cookies', 'dataService', function ($rootScope, $scope, $cookies, dataService) {
    $rootScope.title = 'Welcome to Shy High Growth';
    $rootScope.description = 'We bring you clients';
    $rootScope.keywords = 'csv,db,shg';
    $scope.login = function (loginuser) {
        dataService.login().exec({}, loginuser).$promise.then(function (res, err) {
            if (res.status.code === 200) {
                $rootScope.loggedUser = res.data;
                $cookies.put('shgauth', res.token);
                $rootScope.changeView('/admin/dashboard');
            }
        });
    };
}]);
