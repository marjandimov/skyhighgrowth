﻿shgApp.factory('flashService', ['$rootScope', '$timeout', '$location', function ($rootScope, $timeout, $location) {
    return {
        ok: function (status) {
            $rootScope.message = 'OK - ' + status.message;
            $rootScope.code = status.code;
            $rootScope.messages = status.messages;
            $('#flash').hide();
            $('#flash').show();

            $timeout(function () {
                $rootScope.code = null;
                $rootScope.message = '';
                $rootScope.messages = [];
                $('#flash').hide();
            }, 2000);

            return null;
        },
        err: function (status) {
            $rootScope.message = 'ERROR - ' + status.message;
            $rootScope.code = status.code;
            $rootScope.messages = status.messages;
            $('#flash').hide();
            $('#flash').show();
            $timeout(function () {
                $rootScope.code = null;
                $rootScope.message = '';
                $rootScope.messages = [];
                $('#flash').hide();
            }, 2000);

            return null;
        }
    };
}]);