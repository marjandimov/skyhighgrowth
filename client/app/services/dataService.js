﻿shgApp.factory('dataService', ['$resource', '$http', '$routeParams', '$rootScope', '$cookies', '$window', 'flashService', 'FileSaver', 'Blob', function ($resource, $http, $routeParams, $rootScope, $cookies, $window, flashService, FileSaver, Blob) {
    return {
        getOwnAccount: function (ctrlName, id, cb) {
            $resource($rootScope.getUrl() + '/api/' + ctrlName + '/own/:id', { id: id }, { exec: { method: 'GET', headers: { 'Authorization': 'Bearer ' + $cookies.get('shgauth') } } }).exec().$promise.then(function (res) {
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return;
            });
        },
        get: function (ctrlName, id, cb) {
            $resource($rootScope.getUrl() + '/api/' + ctrlName + '/:id', { id: id }, { exec: { method: 'GET', headers: { 'Authorization': 'Bearer ' + $cookies.get('shgauth') } } }).exec().$promise.then(function (res) {
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return;
            });
        },
        getAllPagged: function (ctrlName, skip, limit, cb) {
            return $resource($rootScope.getUrl() + '/api/' + ctrlName, { skip: skip, limit: limit }, { exec: { method: 'GET', headers: { 'Authorization': 'Bearer ' + $cookies.get('shgauth') } } }).exec().$promise.then(function (res) {
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return res;
            });
        },
        getAll: function (ctrlName, cb) {
            return $resource($rootScope.getUrl() + '/api/' + ctrlName, {}, { exec: { method: 'GET', headers: { 'Authorization': 'Bearer ' + $cookies.get('shgauth') } } }).exec().$promise.then(function (res) {
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return res;
            });
        },
        create: function (ctrlName, obj, cb) {

            return $resource($rootScope.getUrl() + '/api/' + ctrlName, {}, { exec: { method: 'POST', headers: { 'Authorization': 'Bearer ' + $cookies.get('shgauth') } } }).exec(obj).$promise.then(function (res) {
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return;
            });
        },
        update: function (ctrlName, obj, cb) {
            return $resource($rootScope.getUrl() + '/api/' + ctrlName + '/' + obj._id, {}, { exec: { method: 'PUT', headers: { 'Authorization': 'Bearer ' + $cookies.get('shgauth') } } }).exec(obj).$promise.then(function (res) {
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return;
            });
        },
        updateOwnAccount: function (ctrlName, obj, cb) {
            return $resource($rootScope.getUrl() + '/api/' + ctrlName + '/own/' + obj._id, {}, { exec: { method: 'PUT', headers: { 'Authorization': 'Bearer ' + $cookies.get('shgauth') } } }).exec(obj).$promise.then(function (res) {
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return;
            });
        },
        delete: function (ctrlName, id, cb) {
            var result = confirm("Are you sure you want to delete?");
            if (result) {
                $resource($rootScope.getUrl() + '/api/' + ctrlName + '/' + id, {}, { exec: { method: 'DELETE', headers: { 'Authorization': 'Bearer ' + $cookies.get('shgauth') } } }).exec().$promise.then(function (res) {
                    $rootScope.status = res.status;
                    if (cb) {
                        cb(res);
                    }
                    return;
                });
            }

        },
        login: function () {
            return $resource($rootScope.getUrl() + '/api/user/login', {}, { exec: { method: 'POST' } });
        },
        register: function () {
            return $resource($rootScope.getUrl() + '/api/user/register', {}, { exec: { method: 'POST' } });
        },
        forgotpassword: function () {
            return $resource($rootScope.getUrl() + '/api/user/forgotpassword', {}, { exec: { method: 'POST' } });
        },
        getLoggedUser: function (cb) {
            $resource($rootScope.getUrl() + '/api/user/loggeduser', {}, { exec: { method: 'GET', headers: { 'Authorization': 'Bearer ' + $cookies.get('shgauth') } } }).exec().$promise.then(function (res) {
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return;
            });
        },
        getAllLists: function (cb) {
            $resource($rootScope.getUrl() + '/api/list/lists', {}, { exec: { method: 'GET' } }).exec().$promise.then(function (res) {
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return;
            });
        },
        search: function (ctrlName, actionName, skip, limit, obj, cb) {
            return $resource($rootScope.getUrl() + '/api/' + ctrlName + '/' + actionName, { skip: skip, limit: limit }, { exec: { method: 'POST', headers: { 'Authorization': 'Bearer ' + $cookies.get('shgauth') } } }).exec(obj).$promise.then(function (res) {
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return;
            });
        },
        exportSelectedDb: function (obj, cb) {
            $rootScope.$broadcast('loading', { show: true });
            return $resource($rootScope.getUrl() + '/api/db/export/selected', {}, {
                exec:
                {
                    method: 'POST',
                    headers: {
                        'Authorization': 'Bearer ' + $cookies.get('shgauth')
                    },
                    responseType: 'arraybuffer',
                    cache: false,
                    transformResponse: function (data) {
                        var ret = new Blob([data], { type: 'text/csv;charset=utf-8' });
                        FileSaver.saveAs(ret, obj.ExportType + '.csv');
                    }
                }
            }).exec(obj).$promise.then(function (res) {
                $rootScope.$broadcast('loading', { show: false });
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return;
            });
        },
        exportSelectedIes: function (obj, cb) {
            $rootScope.$broadcast('loading', { show: true });
            return $resource($rootScope.getUrl() + '/api/ies/export/selected', {}, {
                exec:
                {
                    method: 'POST',
                    headers: {
                        'Authorization': 'Bearer ' + $cookies.get('shgauth')
                    },
                    responseType: 'arraybuffer',
                    cache: false,
                    transformResponse: function (data) {
                        var ret = new Blob([data], { type: 'text/csv;charset=utf-8' });
                        FileSaver.saveAs(ret, obj.ExportType + '.csv');
                    }
                }
            }).exec().$promise.then(function (res) {
                $rootScope.$broadcast('loading', { show: false });
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return;
            });
        },
        exportVerifiedEmails: function (cb) {
            $rootScope.$broadcast('loading', { show: true });
            return $resource($rootScope.getUrl() + '/api/email/export', {}, {
                exec:
                {
                    method: 'GET',
                    headers: {
                        'Authorization': 'Bearer ' + $cookies.get('shgauth')
                    },
                    responseType: 'arraybuffer',
                    cache: false,
                    transformResponse: function (data) {
                        var ret = new Blob([data], { type: 'text/csv;charset=utf-8' });
                        FileSaver.saveAs(ret, 'verified-emails.csv');
                    }
                }
            }).exec({}).$promise.then(function (res) {
                $rootScope.$broadcast('loading', { show: false });
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return;
            });
        },
        exportAllSearch: function (obj, cb) {
            $rootScope.$broadcast('loading', { show: true });
            return $resource($rootScope.getUrl() + '/api/ies/export', {}, {
                exec:
                {
                    method: 'POST',
                    headers: {
                        'Authorization': 'Bearer ' + $cookies.get('shgauth')
                    },
                    responseType: 'arraybuffer',
                    cache: false,
                    transformResponse: function (data) {
                        var ret = new Blob([data], { type: 'text/csv;charset=utf-8' });
                        FileSaver.saveAs(ret, obj.SearchType + '.csv');
                    }
                }
            }).exec(obj).$promise.then(function (res) {
                $rootScope.$broadcast('loading', { show: false });
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return;
            });
        },
        filters: function (ctrlName, actionName, obj, cb) {
            return $resource($rootScope.getUrl() + '/api/' + ctrlName + '/' + actionName, {}, { exec: { method: 'GET', headers: { 'Authorization': 'Bearer ' + $cookies.get('shgauth') } } }).exec(obj).$promise.then(function (res) {
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return;
            });
        },
        contactus: function (obj, cb) {
            return $resource($rootScope.getUrl() + '/api/user/contactus', {}, { exec: { method: 'POST' } }).exec(obj).$promise.then(function (res) {
                $rootScope.status = res.status;
                if (cb) {
                    cb(res);
                }
                return;
            });
        },
        downloadSample: function (obj, cb) {
            $http({
                url: '/api/list/downloadsample',
                method: 'POST',
                data: {
                    id: obj.id
                },
                responseType: 'blob'
            })
                .then(function (data) {
                    console.log(data);

                    var ret = new Blob([data.data], { type: 'text/csv;charset=utf-8' });
                    FileSaver.saveAs(ret, 'Sample.csv');

                }).catch(function (response) {
            console.log(response);
            console.log('Unable to download the file');
        });
        },
    }
}]);

