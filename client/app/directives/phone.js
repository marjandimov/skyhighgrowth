﻿shgApp.directive('phone', ['$parse', function ($parse) {
    return {
        require: ['^form', 'ngModel'],
        scope: true,
        link: function (scope, elem, attrs, ctrls) {
            function formatFn(input, format, sep) {
                var output = "";
                var idx = 0;
                for (var i = 0; i < format.length && idx < input.length; i++) {
                    output += input.substr(idx, format[i]);
                    if (idx + format[i] < input.length) output += sep;
                    idx += format[i];
                }
                output += input.substr(idx);
                return output;
            }
            
            //elem.keypress(function (evt) {
            //    if (evt.which < 48 || evt.which > 57) {
            //        evt.preventDefault();
            //    }
            //});
            //elem.keyup(function () {
            //    var repl = elem.val().replace(/-/g, ""); // remove hyphens
            //    if (repl.length > 0) {
            //        repl = formatFn(repl, [3, 3], "-");
            //    }

            //    elem.val(repl);
            //});

            ctrls[1].$validators.phone = function (modelValue, viewValue) {
                if (modelValue) {
                    scope.regExp = new RegExp('\\+?\\d{3}[- ]?\\d{3}[- ]?\\d{4}$');
                    var test = scope.regExp.test(elem.val());
                    ctrls[1].$setValidity('phone', test);
                    return test;
                }

                ctrls[1].$setValidity('phone', true);
                return true;
            };
        }
    };
}]);