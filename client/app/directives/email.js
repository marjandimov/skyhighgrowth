﻿shgApp.directive('email', ['$parse', function ($parse) {
    return {
        require: ['^form', 'ngModel'],
        scope: true,
        link: function (scope, elem, attrs, ctrls) {
            ctrls[1].$validators.email = function (modelValue, viewValue) {
                if (modelValue) {
                    scope.regExp = new RegExp('(([^<>()[\\]\\\\.,;:\\s@\\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\\"]+)*)|(\\".+\\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$');
                    var test = scope.regExp.test(elem.val());
                    ctrls[1].$setValidity('email', test);
                    return test;
                }

                ctrls[1].$setValidity('email', true);
                return true;
            };
        }
    };
}])