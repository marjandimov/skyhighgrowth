shgApp.directive('myCustomDirective', ['$interpolate', function($interpolate) {
    return {
        compile: function(tElement, tAttributes) {
            console.log(tAttributes.text + ' in compile');
            return {
                pre: function(scope, iElem, iAttrs, controller) {
                    console.log(iAttrs.text + ' in pre()');
                },
                post: function(scope, iElem, iAttrs, controller) {
                    console.log(iAttrs.text +  ' in post()');
                    iElem.css('border', '1px solid black');
                    if(iAttrs.addclick === 'true') {
                        iElem.on('click', scope.clicked);
                    }

                }
            };
        },
        link: function(scope, element, attrs) {
            console.log(attrs.text + ' in link')
        },
        controller: function($scope, $element, $attrs) {
            //console.log($attrs.text + ' in controller');
            var txt = $interpolate($attrs.text)($scope);

            $scope.clicked = function () {
                alert(txt + ' is clicked')
            }
        }
    };
}]);