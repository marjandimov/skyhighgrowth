﻿var shgApp = angular.module('shgApp', ['ngRoute', 'ngResource', 'ngAnimate', 'ngCookies', 'ngSanitize', 'ngMeta', 'ngFileUpload', 'ngServerPaginate', 'ngAccordion', 'ui.tinymce', 'ngFileSaver', 'ngCsv']);
shgApp.constant('_', window._);
shgApp.config(['$routeProvider', '$httpProvider', '$locationProvider', '$qProvider', '$compileProvider', 'ngMetaProvider', function ($routeProvider, $httpProvider, $locationProvider, $qProvider, $compileProvider, ngMetaProvider) {
    //angular.lowercase = angular.$$lowercase;
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|blob):/);
    $httpProvider.interceptors.push('customHttpInterceptor');
    $qProvider.errorOnUnhandledRejections(false);
    //$locationProvider.html5Mode(true);
    ngMetaProvider.useTitleSuffix(true);

    $routeProvider
            .when('/',
            {
                controller: 'homeCtrl',
                templateUrl: 'app/views/home.html',
                resolve: {}
            })
            .when('/contact',
            {
                controller: 'contactCtrl',
                templateUrl: 'app/views/contact.html',
                resolve: {}
            })
            .when('/about',
            {
                controller: 'aboutCtrl',
                templateUrl: 'app/views/about.html',
                resolve: {}
            })
            .when('/csv',
            {
                controller: 'csvCtrl',
                templateUrl: 'app/views/csv.html',
                resolve: {}
            })
            .when('/login',
            {
                controller: 'loginCtrl',
                templateUrl: 'app/views/login.html',
                resolve: {}
            })
            .when('/premium',
            {
                controller: 'premiumCtrl',
                templateUrl: 'app/views/premium.html',
                resolve: {}
            })
            .when('/register',
            {
                controller: 'registerCtrl',
                templateUrl: 'app/views/register.html',
                resolve: {}
            })
            .when('/forgotpassword',
            {
                controller: 'forgotpasswordCtrl',
                templateUrl: 'app/views/forgotpassword.html',
                resolve: {}
            })
            .when('/shgdb',
            {
                controller: 'shgdbCtrl',
                templateUrl: 'app/views/shgdb.html',
                resolve: {}
            })
            .when('/admin/dashboard',
            {
                controller: 'dashboardCtrl',
                templateUrl: 'app/views/admin/dashboard.html',
                resolve: {}
            })
            .when('/admin/group',
            {
                controller: 'groupCtrl',
                templateUrl: 'app/views/admin/group.html',
                resolve: {}
            })
            .when('/admin/list',
            {
                controller: 'listCtrl',
                templateUrl: 'app/views/admin/list.html',
                resolve: {}
            })
            .when('/admin/file',
            {
                controller: 'fileCtrl',
                templateUrl: 'app/views/admin/file.html',
                resolve: {}
            })
            .when('/admin/payment',
            {
                controller: 'paymentCtrl',
                templateUrl: 'app/views/admin/payment.html',
                resolve: {}
            })
            .when('/admin/user',
            {
                controller: 'userCtrl',
                templateUrl: 'app/views/admin/user.html',
                resolve: {}
            })
            .when('/admin/account',
            {
                controller: 'accountCtrl',
                templateUrl: 'app/views/admin/account.html',
                resolve: {}
            })
            .when('/admin/order',
            {
                controller: 'orderCtrl',
                templateUrl: 'app/views/admin/order.html',
                resolve: {}
            })
            .when('/admin/ies',
            {
                controller: 'iesCtrl',
                templateUrl: 'app/views/admin/ies.html',
                resolve: {}
            })
            .when('/admin/db',
            {
                controller: 'dbCtrl',
                templateUrl: 'app/views/admin/db.html',
                resolve: {}
            })
            .when('/admin/verifyemail',
            {
                controller: 'verifyEmailCtrl',
                templateUrl: 'app/views/admin/verifyemail.html',
                resolve: {}
            })
            .otherwise({redirectTo: '/'});
}]);
shgApp.factory('customHttpInterceptor', ['$q', '$rootScope', '$location', 'flashService', function ($q, $rootScope, $location, flashService) {
    return {
        request: function (config) {
            if (config.url) {
                if (config.url.startsWith($rootScope.getUrl()) && !config.url.endsWith('.html')) {

                    if ($rootScope.reqCount == 0) {
                        $rootScope.$broadcast('loading', { show: true });
                    }
                    $rootScope.reqCount++;
                }
            }
            return config || $q.when(config);
        },
        requestError: function (rejection) {
            return $q.reject(rejection);
        },
        response: function (response) {

            if (response.data) {
                if (typeof (response.data) === 'object') {

                    if (response.status) {
                        if (response.status === 200) {

                            $rootScope.reqCount--;
                            if ($rootScope.reqCount == 0) {
                                $rootScope.$broadcast('loading', { show: false });
                                flashService.ok(response.data.status);
                            }
                        }
                    }
                    else {
                        $rootScope.$broadcast('loading', { show: false });
                    }
                }
            }
            return response || $q.when(response);
        },
        responseError: function (rejection) {
            if (rejection.status) {
                if (rejection.status === 200) {
                    $rootScope.$broadcast('loading', { show: false });
                    flashService.ok(rejection.data.status);
                }
                else if (rejection.status === 500) {
                    $rootScope.$broadcast('loading', { show: false });
                    flashService.err(rejection.data.status);
                }
                else if (rejection.status === 404) {
                    $rootScope.$broadcast('loading', { show: false });
                    flashService.err(rejection.data.status);
                }
                else if (rejection.status === 403) {
                    $rootScope.$broadcast('loading', { show: false });
                    flashService.err(rejection.data.status);
                }
                else if (rejection.status === 401) {
                    $rootScope.$broadcast('loading', { show: false });
                    flashService.err(rejection.data.status);
                    $location.path('/login');
                }
                else {
                    flashService.err({ code: 406, title: 'Status error', messages: { message: 'Unknown error' } });
                }
            }
            return $q.reject(rejection);
        }
    };
}]);
shgApp.run(['$rootScope', '$window', '$location', '$cookies', 'ngMeta', function ($rootScope, $window, $location, $cookies, ngMeta) {

    $rootScope.alert = window.alert;

    $rootScope.reqCount = 0;
    $rootScope.loggedUser;
    $rootScope.menu = {
        menuToggle: false
    };

    $rootScope.title = 'Welcome to Shy High Growth';
    $rootScope.description = 'We bring you clients';
    $rootScope.keywords = 'csv,db,shg';

    ngMeta.init();

    $rootScope.databaseLink = function () {
        if ($rootScope.loggedUser) {
            if ($rootScope.isInRole('member')) {
                $rootScope.changeView('/admin/db');
            }
            else if ($rootScope.isInRole('administrator')) {
                $rootScope.changeView('/admin/ies');
            }
        }
        else {
            $rootScope.changeView('/login');
        }
    };

    $rootScope.getUrl = function () {
        return 'http://' + $location.host() + ':' + $location.port(); 
    };

    $rootScope.back = function () {
        $window.history.back();
    };
    $rootScope.isInRole = function (role) {
        if ($rootScope.loggedUser) {
            if ($rootScope.loggedUser.role) {
                if ($rootScope.loggedUser.role.name === role) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else { return false; }
        }
        else {
            return false;
        }
    };

    $rootScope.logout = function () {
        $rootScope.loggedUser = void (0);
        $cookies.remove('shgauth');
        $rootScope.changeView('/');
    };

    $rootScope.changeView = function (view) {
        $location.path(view);
    };

    $rootScope.$on('loading', function (event, data) {
        $rootScope.loading = data.show;
    });

    $rootScope.$on('$locationChangeStart', function () {
        if (!$rootScope.loggedUser) {
            if ($location.path().startsWith('/admin/')) {
                $rootScope.changeView('/');
            }
        }
    });

    $rootScope.$on('$viewContentLoaded', function () {

    });

    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        ngMeta.setTitle('Sky High Growth', ' - ' + $rootScope.title);
        ngMeta.setTag('description', $rootScope.description);
        ngMeta.setTag('keywords', $rootScope.keywords);
    });

    //--- Helper functions------------------------------------------------
    $rootScope.lodashRemoveBy = function (array, propertyName, propertyValue) {
        return _.remove(array, [propertyName, propertyValue]);
    };
    $rootScope.lodashFindBy = function (array, propertyName, propertyValue) {
        return _.find(array, [propertyName, propertyValue]);
    };
    $rootScope.lodashFilterBy = function (array, propertyName, propertyValue) {
        return _.filter(array, [propertyName, propertyValue]);
    };
    $rootScope.lodashSortB = function (array, propertyName) {
        return _.sortBy(array, [propertyName]);
    };
    $rootScope.findBy = function (array, propertyName, propertyValue) {
        if (array) {
            if (array.length != 0) {
                for (var i = 0; i < array.length; i++) {
                    if (array[i][propertyName] === propertyValue) {
                        return array[i];
                    }
                }
            }
        }
        return null;
    };
    $rootScope.getCurrentPage = function () {
        //return $location.path();
        if ($location.path() === '/') {
            return 'home';
        } else {
            var tmp = $location.path().split('/');
            return tmp[tmp.length - 1];
        }
    };
    $rootScope.getCurrentPageTitle = function () {
        if ($location.path() === '/') {
            return 'Bring the Best Clients to Your Doorstep';
        }
        if ($location.path() === '/premium') {
            return 'Get more deals and boost your business.';
        }
        if ($location.path() === '/shgdb') {
            return 'Get instant access to hundreds of thousands of hot contacts.';
        }
        if ($location.path() === '/csv') {
            return 'Predefined lists';
        }
        if ($location.path() === '/register') {
            return 'Sign up';
        }
        if ($location.path() === '/login') {
            return 'Login';
        } 
        if ($location.path() === '/contact') {
            return 'Contact us';
        } 
    }

    $rootScope.getAllEntities = function (arr) {
        return _.orderBy(_.map(arr, function (item, index) {
            return { Name: item, Value: item }
        }), ['Name'], ['asc']);
    };
    $rootScope.generateGUID = function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    };
    $rootScope.alert = function (msg) {
        $window.alert(msg);
    };
    //---------------------------------------------------------------
}]);