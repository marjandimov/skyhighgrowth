//-----------------------------------------------------------------------------------------------------
var crypto = require('crypto');
var cli = require('cli-color');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var async = require('async');
var _ = require('lodash');
var fs = require('fs');
var path = require('path');
var dirTree = require('directory-tree');
var cfg = require('../config/serverCfg');
var nodemailer = require('nodemailer');
var csv = require('fast-csv');

var transporter = nodemailer.createTransport({
    host: 'smtp.sendgrid.net',
    port: 587,
    secure: false,
    auth: {
        user: 'apikey',
        pass: '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'
    }
});
//-----------------------------------------------------------------------------------------------------
exports.handleDbError = function (error) {
    console.log(error.name);
    console.log(error.code);
    if (error.name === 'MongoError' && error.code === 11000) {
        //next(new Error('There was a duplicate key error'));
    } else {
        //next(error);
    }
};

//-----------------------------------------------------------------------------------------------------
exports.getTransporter = function () {
    var transporter;
    if (cfg.production) {
        transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: cfg.production_settings.mail.user,
                pass: cfg.production_settings.mail.pass
            }
        });
    }
    else {
         transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: cfg.development_settings.mail.user,
                pass: cfg.development_settings.mail.pass
            }
        });
    }
    return transporter;
}
//-----------------------------------------------------------------------------------------------------
exports.isObjectId = function (str) {
    return mongoose.Types.ObjectId.isValid(str);
};
exports.replaceAll = function (str, search, replacement) {
    return str.split(search).join(replacement);
};
//-----------------------------------------------------------------------------------------------------
exports.capitalize = function (str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
};
//-----------------------------------------------------------------------------------------------------
exports.getRequestIp = function(req) {
    return req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        req.connection.socket.remoteAddress;
};
//-----------------------------------------------------------------------------------------------------
exports.getSocketIp = function(socket) {
    return socket.handshake.address;
};
//-----------------------------------------------------------------------------------------------------
exports.dateFormat = function(date, fstr, utc) {
    utc = utc ? 'getUTC' : 'get';
    return fstr.replace(/%[YmdHMS]/g, function (m) {
        switch (m) {
            case '%Y': return date[utc + 'FullYear'] (); // no leading zeros required
            case '%m': m = 1 + date[utc + 'Month'] (); break;
            case '%d': m = date[utc + 'Date'] (); break;
            case '%H': m = date[utc + 'Hours'] (); break;
            case '%M': m = date[utc + 'Minutes'] (); break;
            case '%S': m = date[utc + 'Seconds'] (); break;
            default: return m.slice (1); // unknown code, remove %
        }
        // add leading zero if required
        return ('0' + m).slice (-2);
    });
}
//-----------------------------------------------------------------------------------------------------
exports.generateHash = function(str) {
    return bcrypt.hashSync(str, bcrypt.genSaltSync(cfg.salt_factor), null);
};
exports.generatePassword = function(howMany, chars) {
    chars = chars
    || "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    var rnd = crypto.randomBytes(howMany)
        , value = new Array(howMany)
        , len = chars.length;

    for (var i = 0; i < howMany; i++) {
        value[i] = chars[rnd[i] % len]
    };

    return value.join('');
}
//-----------------------------------------------------------------------------------------------------
exports.validateHash = function(str, hash) {
    return bcrypt.compareSync(str, hash);
};
//-----------------------------------------------------------------------------------------------------
exports.extractTokenFromHeader = function (req) {
    if (req.headers && req.headers.authorization) {
        var parts = req.headers.authorization.split(' ');
        
        if (parts.length === 2 && parts[0] === 'Bearer') {
            return parts[1];
        } else {
            return null;
        }
    } else {
        return null;
    }
};
//-----------------------------------------------------------------------------------------------------
exports.logMessageToConsoleBlue = function(msg) {
    if(cfg.log_to_console) {
        console.log(cli.blue('---> ') + cli.blue(msg));
    }
};
exports.logMessageToConsoleRed = function(msg) {
    if(cfg.log_to_console) {
        console.log(cli.red('---> ') + cli.red(msg));
    }
};
exports.logMessageToConsoleGreen = function(msg) {
    if(cfg.log_to_console) {
        console.log(cli.green('---> ') + cli.green(msg));
    }
};
exports.logMessageToConsoleYellow = function(msg) {
    if(cfg.log_to_console) {
    console.log(cli.yellow('---> ') + cli.yellow(msg));
    }
};
exports.logReqToConsole = function(req) {
    if(cfg.log_to_console) {
        console.log(cli.blue('---> ') + cli.green('[' + req.method + ' ' + req.path + ']') + cli.blue(' -> ') + cli.green('[model:' + req.options.model + ']' + cli.blue(' -> ') + cli.green('[method:' + req.options.controller + ']') + cli.blue(' -> ') + cli.green('[action:' + req.options.action + ']')));
    }
};
exports.logSockToConsole = function(req, action, msg) {
    if(cfg.log_to_console) {
        console.log(cli.blue('---> ') + cli.green('[' + req.method + ' ' + req.path + ']') + cli.blue(' -> ') + cli.green('[action:' + action + ']' + cli.blue(' -> ') + cli.green('[' + msg + ']')));
    }
};
exports.logUnAuthToConsole = function(req) {
    if(cfg.log_to_console) {
        console.log(cli.blue('---> ') + cli.green('[' + req.method + ' ' + req.path + ']') + cli.blue(' -> ') + cli.green('[') + cli.red('401 Unauthorized') +  cli.green(']'));
    }
};
exports.logErrorToConsole = function(err) {
    if(cfg.log_to_console) {
        console.log(cli.blue('---> ') + cli.red('ERROR -> ') + cli.red(err));
    }
};
//-----------------------------------------------------------------------------------------------------
exports.encrypt = function(data) {
    var cipher = crypto.createCipher("aes256", cfg.encrypt_key);
    var str = cipher.update(data, "utf8", "base64") + cipher.final("base64");
    return str;
};
exports.decrypt = function(data) {
    var decipher = crypto.createDecipher("aes256", cfg.encrypt_key);
    str = decipher.update(data, "base64", "utf8") + decipher.final("utf8");
    return str;
};
//-----------------------------------------------------------------------------------------------------
exports.getIndexById = function(arr, property, id){
    return _.map(_.pluck(JSON.parse(JSON.stringify(arr)), property), function(e) { return e._id; }).indexOf(id);
};
exports.getObjectByKeyVal = function(arr, objKeyVal) {
    return _.find(JSON.parse(JSON.stringify(arr)), objKeyVal);
};
exports.getIndexByKeyVal = function(arr, key, val) {
    return _.indexOf(_.pluck(JSON.parse(JSON.stringify(arr)), key), val);
};
exports.isExist = function(arr, id) {
    return _.contains(arr, _.find(arr, { _id: id }));
};
exports.getDirectories = function(startPath) {
    //return dirTree(srcpath, ['.jpg', '.jpeg', '.png', '.txt', '.pdf', '.csv', '.xls', '.xlsx']);
    var ret = [];
    var dirs = fs.readdirSync(startPath);

    for (var i in dirs) {
        var currentDir = startPath + '/' + dirs[i];
        var statsDir = fs.statSync(currentDir);

        if (statsDir.isDirectory()) {
            var tmpDir = {
                name: dirs[i],
                size: statsDir.size,
                created: statsDir.ctime,
                modified: statsDir.mtime,
                files: []
            };
            var files = fs.readdirSync(currentDir);
            
            var filesizeSum = 0;
            for(var ii in files) {
                
                var statsFiles = fs.statSync(currentDir + '/' + files[ii]);
                if(statsFiles.isFile()) {
                    var tmpFile = {
                        name: files[ii],
                        size: statsFiles.size,
                        created: statsFiles.ctime,
                        modified: statsFiles.mtime
                    }
                    filesizeSum += statsFiles.size;
                    tmpDir.files.push(tmpFile);
                }
            }
            tmpDir.size = filesizeSum;
            ret.push(tmpDir);
        }
    }
    return ret;
};
exports.countCsvRows = function (path) {
    var cnt = 1;
    csv.fromPath(path)
    .on('data', function (data) {
        cnt += 1
    })
    .on('end', () => {
        return cnt;
    });
}
exports.getFiles = function (path) {
    var ret = [];
    var statsDir = fs.statSync(path);

    if (statsDir.isDirectory()) {
        var files = fs.readdirSync(path);
        for (var i in files) {
            var statsFiles = fs.statSync(path + '/' + files[i]);

            if (statsFiles.isFile()) {
                var tmpFile = {
                    name: files[i],
                    size: statsFiles.size,
                    count: fs.readFileSync(path + '/' + files[i]).toString().split('\n').length - 2, // -header -1 for length 0
                    created: statsFiles.ctime,
                    modified: statsFiles.mtime
                }
                ret.push(tmpFile);
            }
        }
    }
    return ret;
};
exports.deleteFolderAndFiles = function (path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file, index) {
            var curPath = path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                utility.deleteFolderAndFiles(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};
exports.filterBy = function (array, propertyName, propertyValue) {
    return _.filter(array, [propertyName, propertyValue]);
};
exports.getPaginatedItems = function(items, page, per_page) {
    var page = page || 1,
        per_page = per_page || 10,
        offset = (page - 1) * per_page,
        paginatedItems = _.rest(items, offset).slice(0, per_page);
    return {
        page: page,
        per_page: per_page,
        total: items.length,
        total_pages: Math.ceil(items.length / per_page),
        data: paginatedItems
    };
};
exports.dateTimeStamp = function () {
    var today = new Date();
    var sToday = (today.getMonth() + 1).toString();
    sToday += '-' + today.getDate().toString();
    sToday += '-' + today.getFullYear().toString();
    sToday += '-' + today.getHours().toString();
    sToday += '-' + today.getMinutes().toString();
    sToday += '-' + today.getSeconds().toString();
    return sToday;
};
exports.generateEmails = function (domain, name) {
    let nameParts = name.split(" ");
    nameParts = nameParts.map(s => s.trim().toLowerCase());
    nameParts = nameParts.filter(s => s.length > 0);

    if (nameParts.length == 1) {
        return [nameParts[0] + "@" + domain];
    }

    let result = [];
    let addResult = function (address) {
        result.push(address + "@" + domain);
    };
    addResult(nameParts[0] + nameParts[1]);
    addResult(nameParts[1] + nameParts[0]);
    addResult(nameParts[0] + "." + nameParts[1]);
    addResult(nameParts[1] + "." + nameParts[0]);
    addResult(nameParts[0][0] + nameParts[1]);
    addResult(nameParts[1][0] + nameParts[0]);
    addResult(nameParts[0]);
    addResult(nameParts[1]);
    return result;
};
exports.sendMail = function sendMail(mailOptions) {
    console.log("sending mail to " + mailOptions.to);

    transporter.sendMail(mailOptions, function (error, info) {
        console.log("sendMail returned.");
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
            console.log(JSON.stringify(info));
            console.log(`Saving message id ${info.messageId} at ${mailOptions.to}`);
            sentEmails.set(mailOptions.to, info.messageId);
        }
    });
};
exports.sendTestEmail = function (to) {
    var mailOptions = {
        from: 'promo@sendgrid.net',
        to: to,
        subject: 'Email checker.',
        text: 'Email checker. Please do not reply.'
    };
    this.sendMail(mailOptions);
};
