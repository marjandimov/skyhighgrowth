//-----------------------------------------------------------------------------------------------------
var utility = require('../utilities/utility');
var jwt = require('jsonwebtoken');
var _ = require('lodash');
var response = require('../utilities/response');
var mongoose = require('mongoose');
//-----------------------------------------------------------------------------------------------------
var Model = mongoose.model('Model');
//-----------------------------------------------------------------------------------------------------
module.exports = {
    //-------------------------------------------------------------------------------------------------
    isAuthorized: function (req, res, next) {
        var token = utility.extractTokenFromHeader(req);
        var decode = jwt.decode(token);

        req.options = {
            uid: decode.uid,
            rid: decode.rid,
            rname: decode.rname,
            ip: decode.reqip,
            username: decode.username,
            email: decode.email,
            firstname: decode.firstname,
            lastname: decode.lastname,
            model: '',
            controller: '',
            method: req.method.toLowerCase(),
            id: req.params.id,
            permission: {},
            filters: [],
            skip: parseInt(req.query.skip) || 0,
            limit: parseInt(req.query.limit) || 0
        };
        //console.log('skip: ' + req.options.skip);
        //console.log('limit: ' + req.options.limit);
        res.pagging = {
            skip: parseInt(req.query.skip) || 0,
            limit: parseInt(req.query.limit) || 0,
            count: parseInt(req.query.count) || 0, 
            pages: parseInt(req.query.pages) || 0,  
        };
        var path = req.path.replace('/api/', '');
        if(path.indexOf('/') != -1) {
            req.options.model = path.substring(0, path.indexOf('/')).toLowerCase();
            req.options.controller = utility.capitalize(req.options.model) + 'Controller';
        }
        else {
            req.options.model = path.toLowerCase();
            req.options.controller = utility.capitalize(req.options.model) + 'Controller';
        }
        Model.findOne({ name: req.options.model, 'permissions.role': req.options.rid }).exec(function (err, data) {

            if (err) { return response.error(res, 500); }
            if (!data) { return response.error(res, 404); }
            req.options.permission = _.find(JSON.parse(JSON.stringify(data.permissions)), { role: req.options.rid });

            //console.log('model: ' + req.options.model + ' ---> ' + req.options.controller);
            //console.log(req.options.permission);

            // findOne

            //console.log('method: ' + req.options.method);
            //console.log(req.options);
            //console.log('url: ' + req.url);

            if(req.options.method == 'get' && req.options.id) {
                req.options.action = 'findOne';

                if(req.options.permission.find_any) {
                    req.options.filters = [];
                }
                else if(req.options.permission.find_role) {
                    req.options.filters = [
                        { fieldName: 'createdByRole', value: req.options.rid }
                    ];
                }
                else if(req.options.permission.find_own) {
                    req.options.filters = [
                        { fieldName: 'createdByUser', value: req.options.uid }
                    ];
                    console.log(req.options.filters);
                }
                else if (req.url === '/api/user/own/' + req.options.uid) {
                    req.options.filters = [];
                }
                else {
                    return response.error(res, 401);
                }
                utility.logReqToConsole(req);
                return next();
            }
            // findAll
            else if(req.options.method == 'get') {
                req.options.action = 'findAll';

                if(req.options.permission.find_any) {
                    req.options.filters = [];
                }
                else if(req.options.permission.find_role) {
                    req.options.filters = [
                        { fieldName: 'createdByRole', value: req.options.rid }
                    ];
                }
                else if(req.options.permission.find_own) {
                    req.options.filters = [
                        { fieldName: 'createdByUser', value: req.options.uid }
                    ];
                }
                else {
                    return response.error(res, 401);
                }
                utility.logReqToConsole(req);
                return next();
            }
            // create
            else if(req.options.method == 'post') {
                req.options.action = 'create';
                req.body.createdAt = Date.now();
                req.body.modifiedAt = Date.now();
                req.body.createdByUser = req.options.uid;
                req.body.modifiedByUser = req.options.uid;
                req.body.createdByRole = req.options.rid;
                req.body.modifiedByRole = req.options.rid;

                if (req.url.startsWith('/api/db/search')) {
                    req.options.filters = [];
                }
                else if (req.url.startsWith('/api/db/export/')) {
                    req.options.filters = [];
                }
                else if (!req.options.permission.create) {
                    return response.error(res, 401);
                }
                utility.logReqToConsole(req);
                return next();
            }
            // update
            else if(req.options.method == 'put') {
                req.options.action = 'update';
                delete req.body.createdByUser;
                delete req.body.createdByRole;
                delete req.body.createdAt;
                delete req.body.modifiedAt;

                req.body.modifiedByUser = req.options.uid;
                req.body.modifiedByRole = req.options.rid;

                if(req.options.permission.update_any) {
                    req.options.filters = [];
                }
                else if(req.options.permission.update_role) {
                    req.options.filters = [
                        { fieldName: 'createdByRole', value: req.options.rid }
                    ];
                }
                else if(req.options.permission.update_own) {
                    req.options.filters = [
                        { fieldName: 'createdByUser', value: req.options.uid }
                    ];
                }
                else if (req.url === '/api/user/own/' + req.options.uid) {
                    req.options.filters = [];
                }
                else {
                    return response.error(res, 401);
                }
                utility.logReqToConsole(req);
                return next();
            }
            // delete
            else if(req.options.method == 'delete') {
                req.options.action = 'delete';

                if(req.options.permission.delete_any) {
                    req.options.filters = [];
                }
                else if(req.options.permission.delete_role) {
                    req.options.filters = [
                        { fieldName: 'createdByRole', value: req.options.rid }
                    ];
                }
                else if(req.options.permission.delete_own) {
                    req.options.filters = [
                        { fieldName: 'createdByUser', value: req.options.uid }
                    ];
                }
                else {
                    return response.error(res, 401);
                }
                utility.logReqToConsole(req);
                return next();
            }
            return response.error(res, 401);
        });
    }
    //-------------------------------------------------------------------------------------------------
};
//-----------------------------------------------------------------------------------------------------