//-----------------------------------------------------------------------------------------------------
var cfg = require('../config/serverCfg');
//-----------------------------------------------------------------------------------------------------

exports.error = function (res, code, data, messages) {
    var msg = '';
    var msgs = [];
    if (code === 200) {
        msg = 'Success';
    }
    else if (code === 500) {
        msg = 'Internal server error';
    }
    else if (code === 404) {
        msg = 'Not found';
    }
    else if (code === 403) {
        msg = 'Forbiden';
    }
    else if (code === 401) {
        msg = 'Unauthorized';
    }
    else {
        msg = 'Unknown';
    }
    if (!Array.isArray(messages)) {
        if (messages) {
            msgs.push(messages);
        }
    }
    else {
        msgs = messages;
    }
    return res.status(code).jsonp({
        data: data || {},
        status: {
            code: code,
            message: msg,
            messages: msgs
        }
    });
};
exports.ok = function (res, code, data, messages) {
    var msg = '';
    var msgs = [];
    if (code === 200) {
        msg = 'Success';
    }
    else if (code === 500) {
        msg = 'Internal server error';
    }
    else if (code === 404) {
        msg = 'Not found';
    }
    else if (code === 403) {
        msg = 'Forbiden';
    }
    else if (code === 401) {
        msg = 'Unauthorized';
    }
    else {
        msg = 'Unknown';
    }
    if (!Array.isArray(messages)) {
        if (messages) {
            msgs.push(messages);
        }
    }
    else {
        msgs = messages;
    }
    if (res.pagging) {
        return res.status(code).jsonp({
            data: data || {},
            status: {
                code: code,
                message: msg,
                messages: msgs,
                count: res.pagging.count,
                limit: res.pagging.limit,
                skip: res.pagging.skip,
                pages: res.pagging.pages
            }
        });
    }
    else {
        return res.status(code).jsonp({
            data: data || {},
            status: {
                code: code,
                message: msg,
                messages: msgs,
                count: null,
                limit: null,
                skip: null,
                pages: null
            }
        });
    }
};
//-----------------------------------------------------------------------------------------------------