
var cfg = require('./config/serverCfg');
var mongoose = require('mongoose');
var async = require('async');
var response = require('./utilities/response');
var _ = require('lodash');

module.exports = {
        filldata: function () {
            // Fill
            var _setup = this;
            _setup.insertSystemRoleAndAccount(function (rid, uid) {
                _setup.insertRoles(rid, uid, function () {
                    _setup.insertAdministratorAccounts(rid, uid, function () {
                        _setup.insertAccounts(rid, uid, function () {
                            _setup.insertModels(rid, uid, function () {
                                _setup.insertPermissions(rid, uid, function () {
                                    //console.log('---> FILL DB FINISHED SUCCESSFULLY.');
                                });
                            });
                        });
                    });
                });
            });

        },
        insertSystemRoleAndAccount: function (cb) {
            var Role = mongoose.model('Role');
            var User = mongoose.model('User');
            var json = {
                name: 'system',
                title: 'System',
                subtitle: '...',
                description: '...'
            };
            Role.create(json, function (err, systemRole) {
                if (err) {
                    console.log('---> SYSTEM ROLE -> population err: ' + err);
                    return;
                }
                json = {
                    username: 'system',
                    email: 'system@skyhighgrowth.com',
                    password: 'system',
                    firstname: 'System',
                    lastname: 'Account',
                    gender: 'Male',
                    phone: '000/000-000',
                    companyname: 'Sky High Growth',
                    active: false,
                    editable: false,
                    role: systemRole._id
                };
                User.create(json, function (err, systemUser) {
                    if (err) {
                        console.log('---> SYSTEM ACCOUNT -> population: ' + err);
                        return;
                    }
                    console.log('---> SYSTEM ROLE AND ACCOUNT created.');

                    cb(systemRole._id, systemUser._id);
                });
            });
        },
        insertAdministratorAccounts: function (rid, uid, cb) {
            var Role = mongoose.model('Role');
            var User = mongoose.model('User');
            Role.findOne({name: 'administrator'}, function (err, role) {
                var json = [
                    {
                        username: 'admin',
                        email: 'marjan.dimov@outlook.com',
                        password: 'Admin1234!@#$',
                        firstname: 'Marjan',
                        lastname: 'Dimov',
                        gender: 'Male',
                        phone: '+381603690050',
                        companyname: 'Sky High Growth',
                        active: true,
                        role: role._id,
                        createdByUser: uid,
                        modifiedByUser: uid,
                        createdByRole: rid,
                        modifiedByRole: rid
                    }];
                async.each(json, function (obj, callback) { //do something with obj if you like
                    User.create(obj, function (err, data) {
                        if (err) {
                            console.log('---> ADMINISTRATOR ACCOUNTS -> population: ' + err);
                            return;
                        }
                        callback();
                    });
                }, function (asyncerr) { // all done
                    if (asyncerr) {
                        console.log('---> ADMINISTRATOR ACCOUNTS -> asyncerr: ' + asyncerr);
                        return;
                    }
                    else {
                        console.log('---> ADMINISTRATOR ACCOUNTS created.');
                        cb();
                    }
                });
            });
        },
        insertAccounts: function (rid, uid, cb) {
            var Role = mongoose.model('Role');
            var User = mongoose.model('User');
            Role.findOne({name: 'member'}, function (err, role) {
                var json = [
                    {
                        username: 'member',
                        email: 'member@skyhighgrowth.com',
                        password: 'Member1234!@#$',
                        firstname: 'Petar',
                        lastname: 'Petrovic',
                        gender: 'Male',
                        phone: '﻿﻿+381601231212',
                        companyname: 'SHG',
                        active: true,
                        role: role._id,
                        createdByUser: uid,
                        modifiedByUser: uid,
                        createdByRole: rid,
                        modifiedByRole: rid
                    }
                ];
                async.each(json, function (obj, callback) { //do something with obj if you like
                    User.create(obj, function (err, data) {
                        if (err) {
                            console.log('---> USER ACCOUNTS -> population: ' + err);
                            return;
                        }
                        callback();
                    });
                }, function (asyncerr) { // all done
                    if (asyncerr) {
                        console.log('---> USER ACCOUNTS -> asyncerr: ' + asyncerr);
                        return;
                    }
                    else {
                        console.log('---> USER ACCOUNTS created.');
                        cb();
                    }
                });

            });
        },
        insertRoles: function (rid, uid, cb) {
            var Role = mongoose.model('Role');
            var json = [
                {
                    name: 'administrator',
                    title: 'Administrator',
                    subtitle: '...',
                    description: '...',
                    createdByUser: uid,
                    modifiedByUser: uid,
                    createdByRole: rid,
                    modifiedByRole: rid
                },
                {
                    name: 'member',
                    title: 'Member',
                    subtitle: '...',
                    description: '...',
                    createdByUser: uid,
                    modifiedByUser: uid,
                    createdByRole: rid,
                    modifiedByRole: rid
                }
            ];
            Role.create(json, function (cerr, data) {
                if (cerr) {
                    console.log('---> ROLES -> population error: ' + cerr);
                    return;
                }
                console.log('---> ROLES created.');
                cb();
            });
        },
        insertModels: function (rid, uid, cb) {
            var Role = mongoose.model('Role');
            var User = mongoose.model('User');
            var Model = mongoose.model('Model');
            var json = [
                {
                    name: 'model',
                    realname: 'Model',
                    title: 'Models',
                    subtitle: '...',
                    description: '...',
                    route: '/model',
                    permissions: [],
                    createdByUser: uid,
                    modifiedByUser: uid,
                    createdByRole: rid,
                    modifiedByRole: rid
                },
                {
                    name: 'user',
                    realname: 'User',
                    title: 'Users',
                    subtitle: '...',
                    description: '...',
                    route: '/user',
                    permissions: [],
                    createdByUser: uid,
                    modifiedByUser: uid,
                    createdByRole: rid,
                    modifiedByRole: rid
                },
                {
                    name: 'role',
                    realname: 'Role',
                    title: 'Roles',
                    subtitle: '...',
                    description: '...',
                    route: '/role',
                    permissions: [],
                    createdByUser: uid,
                    modifiedByUser: uid,
                    createdByRole: rid,
                    modifiedByRole: rid
                },
                {
                    name: 'group',
                    realname: 'Group',
                    title: 'Groups',
                    subtitle: '...',
                    description: '...',
                    route: '/group',
                    permissions: [],
                    createdByUser: uid,
                    modifiedByUser: uid,
                    createdByRole: rid,
                    modifiedByRole: rid
                },
                {
                    name: 'list',
                    realname: 'List',
                    title: 'Lists',
                    subtitle: '...',
                    description: '...',
                    route: '/list',
                    permissions: [],
                    createdByUser: uid,
                    modifiedByUser: uid,
                    createdByRole: rid,
                    modifiedByRole: rid
                },
                {
                    name: 'file',
                    realname: 'File',
                    title: 'Files',
                    subtitle: '...',
                    description: '...',
                    route: '/file',
                    permissions: [],
                    createdByUser: uid,
                    modifiedByUser: uid,
                    createdByRole: rid,
                    modifiedByRole: rid
                },
                {
                    name: 'payment',
                    realname: 'Payment',
                    title: 'Payments',
                    subtitle: '...',
                    description: '...',
                    route: '/payment',
                    permissions: [],
                    createdByUser: uid,
                    modifiedByUser: uid,
                    createdByRole: rid,
                    modifiedByRole: rid
                },
                {
                    name: 'company',
                    realname: 'Company',
                    title: 'Companies',
                    subtitle: '...',
                    description: '...',
                    route: '/company',
                    permissions: [],
                    createdByUser: uid,
                    modifiedByUser: uid,
                    createdByRole: rid,
                    modifiedByRole: rid
                },
                {
                    name: 'employee',
                    realname: 'Employee',
                    title: 'Employees',
                    subtitle: '...',
                    description: '...',
                    route: '/employee',
                    permissions: [],
                    createdByUser: uid,
                    modifiedByUser: uid,
                    createdByRole: rid,
                    modifiedByRole: rid
                },
                {
                    name: 'ies',
                    realname: 'Import/Export/Search',
                    title: 'Import/Export/Search',
                    subtitle: '...',
                    description: '...',
                    route: '/ies',
                    permissions: [],
                    createdByUser: uid,
                    modifiedByUser: uid,
                    createdByRole: rid,
                    modifiedByRole: rid
                },
                {
                    name: 'db',
                    realname: 'SHG Database',
                    title: 'SHG Database',
                    subtitle: '...',
                    description: '...',
                    route: '/db',
                    permissions: [],
                    createdByUser: uid,
                    modifiedByUser: uid,
                    createdByRole: rid,
                    modifiedByRole: rid
                }
            ];
            async.each(json, function (obj, callback) { //do something with obj if you like
                    Model.create(obj, function (err, data) {
                        if (err) {
                            console.log('---> MODEL -> population: ' + err);
                            return;
                        }
                        callback();
                    });
                },
                function (asyncerr) { // all done
                    if (asyncerr) {
                        console.log('---> MODEL -> asyncerr: ' + asyncerr);
                        return;
                    }
                    else {
                        console.log('---> MODELS created.');
                        cb();
                    }
                });
        },
        insertPermissions: function (rid, uid, cb) {
            var Role = mongoose.model('Role');
            var User = mongoose.model('User');
            var Model = mongoose.model('Model');

            Model.find({}, function (err, models) {
                Role.find({}).where('name').ne('system').exec(function (err, roles) {
                    models.forEach(function (model) {
                        async.each(roles, function (role, callback) {

                            if (role.name == 'administrator' && model.name == 'user') {
                                model.permissions.push({
                                    role: role._id,
                                    view: true,
                                    create: true,
                                    find_any: true,
                                    update_any: true,
                                    delete_any: true,
                                    find_role: true,
                                    update_role: true,
                                    delete_role: true,
                                    find_own: true,
                                    update_own: true,
                                    delete_own: true
                                });
                            }
                            else if (role.name == 'administrator' && model.name == 'model') {
                                model.permissions.push({
                                    role: role._id,
                                    view: true,
                                    create: true,
                                    find_any: true,
                                    update_any: true,
                                    delete_any: true,
                                    find_role: true,
                                    update_role: true,
                                    delete_role: true,
                                    find_own: true,
                                    update_own: true,
                                    delete_own: true
                                });
                            }
                            else if (role.name == 'administrator' && model.name == 'role') {
                                model.permissions.push({
                                    role: role._id,
                                    view: true,
                                    create: true,
                                    find_any: true,
                                    update_any: true,
                                    delete_any: true,
                                    find_role: true,
                                    update_role: true,
                                    delete_role: true,
                                    find_own: true,
                                    update_own: true,
                                    delete_own: true
                                });
                            }
                            else if (role.name == 'administrator' && model.name == 'group') {
                                model.permissions.push({
                                    role: role._id,
                                    view: true,
                                    create: true,
                                    find_any: true,
                                    update_any: true,
                                    delete_any: true,
                                    find_role: true,
                                    update_role: true,
                                    delete_role: true,
                                    find_own: true,
                                    update_own: true,
                                    delete_own: true
                                });
                            }
                            else if (role.name == 'administrator' && model.name == 'list') {
                                model.permissions.push({
                                    role: role._id,
                                    view: true,
                                    create: true,
                                    find_any: true,
                                    update_any: true,
                                    delete_any: true,
                                    find_role: true,
                                    update_role: true,
                                    delete_role: true,
                                    find_own: true,
                                    update_own: true,
                                    delete_own: true
                                });
                                //model.save();
                            }
                            else if (role.name == 'administrator' && model.name == 'payment') {
                                model.permissions.push({
                                    role: role._id,
                                    view: true,
                                    create: true,
                                    find_any: true,
                                    update_any: true,
                                    delete_any: true,
                                    find_role: true,
                                    update_role: true,
                                    delete_role: true,
                                    find_own: true,
                                    update_own: true,
                                    delete_own: true
                                });
                            }
                            else if (role.name == 'administrator' && model.name == 'file') {
                                model.permissions.push({
                                    role: role._id,
                                    view: true,
                                    create: true,
                                    find_any: true,
                                    update_any: true,
                                    delete_any: true,
                                    find_role: true,
                                    update_role: true,
                                    delete_role: true,
                                    find_own: true,
                                    update_own: true,
                                    delete_own: true
                                });
                            }
                            else if (role.name == 'administrator' && model.name == 'company') {
                                model.permissions.push({
                                    role: role._id,
                                    view: true,
                                    create: true,
                                    find_any: true,
                                    update_any: true,
                                    delete_any: true,
                                    find_role: true,
                                    update_role: true,
                                    delete_role: true,
                                    find_own: true,
                                    update_own: true,
                                    delete_own: true
                                });
                            }
                            else if (role.name == 'administrator' && model.name == 'employee') {
                                model.permissions.push({
                                    role: role._id,
                                    view: true,
                                    create: true,
                                    find_any: true,
                                    update_any: true,
                                    delete_any: true,
                                    find_role: true,
                                    update_role: true,
                                    delete_role: true,
                                    find_own: true,
                                    update_own: true,
                                    delete_own: true
                                });
                            }
                            else if (role.name == 'administrator' && model.name == 'ies') {
                                model.permissions.push({
                                    role: role._id,
                                    view: true,
                                    create: true,
                                    find_any: true,
                                    update_any: true,
                                    delete_any: true,
                                    find_role: true,
                                    update_role: true,
                                    delete_role: true,
                                    find_own: true,
                                    update_own: true,
                                    delete_own: true
                                });
                            }
                            else if (role.name == 'administrator' && model.name == 'db') {
                                model.permissions.push({
                                    role: role._id,
                                    view: true,
                                    create: true,
                                    find_any: true,
                                    update_any: true,
                                    delete_any: true,
                                    find_role: true,
                                    update_role: true,
                                    delete_role: true,
                                    find_own: true,
                                    update_own: true,
                                    delete_own: true
                                });
                            }

                            else if (role.name == 'member' && model.name == 'user') {
                                model.permissions.push({
                                    role: role._id,
                                    view: false,
                                    create: false,
                                    find_any: false,
                                    update_any: false,
                                    delete_any: false,
                                    find_role: false,
                                    update_role: false,
                                    delete_role: false,
                                    find_own: false,
                                    update_own: false,
                                    delete_own: false
                                });
                            }
                            else if (role.name == 'member' && model.name == 'model') {
                                model.permissions.push({
                                    role: role._id,
                                    view: false,
                                    create: false,
                                    find_any: false,
                                    update_any: false,
                                    delete_any: false,
                                    find_role: false,
                                    update_role: false,
                                    delete_role: false,
                                    find_own: false,
                                    update_own: false,
                                    delete_own: false
                                });
                            }
                            else if (role.name == 'member' && model.name == 'role') {
                                model.permissions.push({
                                    role: role._id,
                                    view: false,
                                    create: false,
                                    find_any: false,
                                    update_any: false,
                                    delete_any: false,
                                    find_role: false,
                                    update_role: false,
                                    delete_role: false,
                                    find_own: false,
                                    update_own: false,
                                    delete_own: false
                                });
                            }
                            else if (role.name == 'member' && model.name == 'group') {
                                model.permissions.push({
                                    role: role._id,
                                    view: false,
                                    create: false,
                                    find_any: false,
                                    update_any: false,
                                    delete_any: false,
                                    find_role: false,
                                    update_role: false,
                                    delete_role: false,
                                    find_own: false,
                                    update_own: false,
                                    delete_own: false
                                });
                            }
                            else if (role.name == 'member' && model.name == 'list') {
                                model.permissions.push({
                                    role: role._id,
                                    view: false,
                                    create: false,
                                    find_any: false,
                                    update_any: false,
                                    delete_any: false,
                                    find_role: false,
                                    update_role: false,
                                    delete_role: false,
                                    find_own: false,
                                    update_own: false,
                                    delete_own: false
                                });
                            }
                            else if (role.name == 'member' && model.name == 'payment') {
                                model.permissions.push({
                                    role: role._id,
                                    view: false,
                                    create: false,
                                    find_any: false,
                                    update_any: false,
                                    delete_any: false,
                                    find_role: false,
                                    update_role: false,
                                    delete_role: false,
                                    find_own: false,
                                    update_own: false,
                                    delete_own: false
                                });
                            }
                            else if (role.name == 'member' && model.name == 'file') {
                                model.permissions.push({
                                    role: role._id,
                                    view: false,
                                    create: false,
                                    find_any: false,
                                    update_any: false,
                                    delete_any: false,
                                    find_role: false,
                                    update_role: false,
                                    delete_role: false,
                                    find_own: false,
                                    update_own: false,
                                    delete_own: false
                                });
                            }
                            else if (role.name == 'member' && model.name == 'company') {
                                model.permissions.push({
                                    role: role._id,
                                    view: false,
                                    create: false,
                                    find_any: false,
                                    update_any: false,
                                    delete_any: false,
                                    find_role: false,
                                    update_role: false,
                                    delete_role: false,
                                    find_own: false,
                                    update_own: false,
                                    delete_own: false
                                });
                            }
                            else if (role.name == 'member' && model.name == 'employee') {
                                model.permissions.push({
                                    role: role._id,
                                    view: false,
                                    create: false,
                                    find_any: false,
                                    update_any: false,
                                    delete_any: false,
                                    find_role: false,
                                    update_role: false,
                                    delete_role: false,
                                    find_own: false,
                                    update_own: false,
                                    delete_own: false
                                });
                            }
                            else if (role.name == 'member' && model.name == 'ies') {
                                model.permissions.push({
                                    role: role._id,
                                    view: false,
                                    create: false,
                                    find_any: false,
                                    update_any: false,
                                    delete_any: false,
                                    find_role: false,
                                    update_role: false,
                                    delete_role: false,
                                    find_own: false,
                                    update_own: false,
                                    delete_own: false
                                });
                            }
                            else if (role.name == 'member' && model.name == 'db') {
                                model.permissions.push({
                                    role: role._id,
                                    view: true,
                                    create: false,
                                    find_any: true,
                                    update_any: false,
                                    delete_any: false,
                                    find_role: true,
                                    update_role: false,
                                    delete_role: false,
                                    find_own: true,
                                    update_own: false,
                                    delete_own: false
                                });
                            }

                            callback();
                        },
                        function (asyncerr) { // all done
                            if (asyncerr) {
                                console.log('---> PERMISSION -> asyncerr: ' + asyncerr);
                                return;
                            }
                            else {
                                console.log('---> PERMISSIONS for model: ' + model.name + ' created.');
                                model.save();
                                cb();
                            }
                        });
                    });
                });
            });
        }
};