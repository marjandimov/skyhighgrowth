/*
 *
 * Sky High Growth
 * 
 */
var cfg = require('./config/serverCfg');
var utility = require('./utilities/utility');
var setup = require('./setup.js');
var fs = require('fs');
var express = require('express');
var fileUpload = require('express-fileupload');
var cors = require('cors');
var https = require('https');
var http = require('http');
var expressJwt = require('express-jwt');
var jwt = require('jsonwebtoken');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var mongoose = require('mongoose');
var _ = require('lodash');
var path = require('path');
var response = require('./utilities/response');

var http_ip;
var http_port;
if (cfg.production) {
    http_ip = cfg.production_settings.ip;
    http_port = cfg.production_settings.port;
}
else {
    http_ip = cfg.development_settings.ip;
    http_port = cfg.development_settings.port;
}
var app = express();
var corsOptions = {};
if (cfg.production) {
    corsOptions = {
        origin: cfg.production_settings.cors_origin
    };
}
else {
    corsOptions = {
        origin: cfg.development_settings.cors_origin
    };
}

app.use(fileUpload({
    limits: { fileSize: cfg.max_upload_size },
}));

app.use(cors(corsOptions));
app.disable('etag');

// DB CONNECTION
if (cfg.production) {
    //mongoose.createConnection(cfg.production_settings.mongodb_connection, { useNewUrlParser: true });
    mongoose.connect(cfg.production_settings.mongodb_connection).then(
        () => { 
            console.log('mongodb connected!');
            },
        err => { 
            console.log(err);
            }
        );
}
else {
    //mongoose.createConnection(cfg.development_settings.mongodb_connection, { useNewUrlParser: true });
    mongoose.set('useNewUrlParser', true);
    mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);
    
    mongoose.connect(cfg.development_settings.mongodb_connection).then(
        () => { 
            console.log('mongodb connected!');
            },
        err => { 
            console.log(err);
            }
        );
}

// CONFIGURATION
// We are going to protect /api routes with JWT
app.use('/api', expressJwt({ secret: cfg.secret_key}).unless({
    path: [
        '/api/user/login',
        '/api/user/register',
        '/api/user/forgotpassword',
        '/api/list/lists',
        //'/api/company/import',
        //'/api/employee/import',
        '/api/employee/filters',
        '/api/user/contactus',
        '/api/list/downloadsample',
        '/api/email/check'
    ]
}));
app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        if (err.inner.name === 'TokenExpiredError') {
            return response.error(res, 401, {}, 'Token expired');
        }
        else if (err.code === 'credentials_required') {
            return response.error(res, 401, {}, 'Authorization header not found');
        }
        else if (err.code === 'invalid_token') {
            return response.error(res, 401, {}, 'Invalid token');
        }
        else {
            return response.error(res, 401, {}, 'Unauthorized');
        }
    }
});

app.use(express.static('../client'));
if (!cfg.production) {
    app.use(morgan('dev')); // log every request to the console
    //app.use(morgan('method[:method] url[:url] status[:status] length[:res[content-length]/1024 bytes] time[:response-time ms]'));
}

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ strict: true }));
app.use(methodOverride()); // simulate DELETE and PUT

var models_path = __dirname + '/models';
fs.readdirSync(models_path).forEach(function (file) {
    if (~file.indexOf('.js')) {
        require(models_path + '/' + file);
    }
});

// SERVER ROUTES
require('./routes/RoleRoute')(app, this);
require('./routes/UserRoute')(app, this);
require('./routes/ModelRoute')(app, this);
require('./routes/GroupRoute')(app, this);
require('./routes/ListRoute')(app, this);
require('./routes/FileRoute')(app, this);
require('./routes/CompanyRoute')(app, this);
require('./routes/EmployeeRoute')(app, this);
require('./routes/ImportExportSearchRoute')(app, this);
require('./routes/DbRoute')(app, this);
require('./routes/EmailRoute')(app, this);

// CONTROLLERS
require('./controllers/RoleController')(this);
require('./controllers/UserController')(this);
require('./controllers/ModelController')(this);
require('./controllers/GroupController')(this);
require('./controllers/ListController')(this);
require('./controllers/FileController')(this);
require('./controllers/CompanyController')(this);
require('./controllers/EmployeeController')(this);
require('./controllers/ImportExportSearchController')(this);
require('./controllers/DbController')(this);

// SERVER LISTEN
var key = fs.readFileSync(__dirname + '/key.pem', 'utf8');
var cert = fs.readFileSync(__dirname + '/cert.pem', 'utf8');

var options = {
    key: key,
    cert: cert
};

//var server = https.createServer(options, app);
var server = http.createServer(app);

server.listen(http_port, http_ip, function () {
    utility.logMessageToConsoleBlue('Server is listening on port ' + http_port);
});

if (cfg.fill_db_on_init) {
    setup.filldata();
}

module.exports = app;
