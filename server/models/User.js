//-----------------------------------------------------------------------------------------------------
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var validator = require('../utilities/validator');
var cfg = require('../config/serverCfg');
var utility = require('../utilities/utility');
//-----------------------------------------------------------------------------------------------------
var userSchema = mongoose.Schema({
    username: { type: String, trim: true, required: 'Username is required', lowercase: true, unique: true },
    email: { type: String, trim: true, lowercase: true, unique: true, required: 'Email required', validate: [validator.email, 'Email is not valid'] },
    password: { type: String, required: 'Password is required' },
    firstname : { type: String, trim: true, required: 'First name is required' },
    lastname : { type: String, trim: true, required: 'Last name is required' },
    gender: { type: String, enum: ['Male','Female'], required: 'Gender is required' },
    phone: { type: String },
    companyname: { type: String },
    active: { type: Boolean },
    loginAttempts: { type: Number, required: 'Login attempts is required', default: 0 },
    lockUntil: { type: Number },
    role: { type: mongoose.Schema.ObjectId, ref: 'Role', required: 'Role is required' },
    //-------------------------------------------------------------------------------------------------
    lastLoginIp: { type: String },
    currentLoginIp: { type: String },
    lastLoginDate: { type: Date, default: Date.now },
    currentLoginDate: { type: Date, default: Date.now },
    //-------------------------------------------------------------------------------------------------
    createdAt: { type: Date, default: Date.now },
    modifiedAt: { type: Date, default: Date.now },
    //-------------------------------------------------------------------------------------------------
    createdByUser: { type: mongoose.Schema.ObjectId, ref: 'User' },
    modifiedByUser: { type: mongoose.Schema.ObjectId, ref: 'User' },
    //-------------------------------------------------------------------------------------------------
    createdByRole: { type: mongoose.Schema.ObjectId, ref: 'Role' },
    modifiedByRole: { type: mongoose.Schema.ObjectId, ref: 'Role' }
    //-------------------------------------------------------------------------------------------------
},
{
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
}
);
//-----------------------------------------------------------------------------------------------------
userSchema.methods.clearFields = function() {
    var obj = this.toObject();
    if(obj.createdByUser) {
        var tmp = obj.createdByUser;
        delete obj.createdByUser;
        obj.createdByUser = {
            username: tmp.username,
            firstname: tmp.firstname,
            lastname: tmp.lastname
        };
    }
    if(obj.modifiedByUser) {
        var tmp = obj.modifiedByUser;
        delete obj.modifiedByUser;
        obj.modifiedByUser = {
            username: tmp.username,
            firstname: tmp.firstname,
            lastname: tmp.lastname
        };
    }
    if(obj.createdByRole) {
        var tmp = obj.createdByRole;
        delete obj.createdByRole;
        obj.createdByRole = {
            name: tmp.name,
            title: tmp.title
        };
    }
    if(obj.modifiedByRole) {
        var tmp = obj.modifiedByRole;
        delete obj.modifiedByRole;
        obj.modifiedByRole = {
            name: tmp.name,
            title: tmp.title
        };
    }
    delete obj.__v;
    return obj;
};
//-----------------------------------------------------------------------------------------------------
userSchema.pre('save', function(next) {
    var obj = this;
    obj.modifiedAt = Date.now();
    // only hash the password if it has been modified (or is new)
    if (!obj.isModified('password')) {
        return next();
    }
    else {
        // generate a salt
        bcrypt.genSalt(cfg.salt_factor, function(err, salt) {
            if (err) return next(err);

            // hash the password using our new salt
            bcrypt.hash(obj.password, salt, null, function(err, hash) {
                if (err) return next(err);

                // override the cleartext password with the hashed one
                obj.password = hash;
                next();
            });
        });
    }
});
//-----------------------------------------------------------------------------------------------------
userSchema.virtual('isLocked').get(function() {
    // check for a future lockUntil timestamp
    return !!(this.lockUntil && this.lockUntil > Date.now());
});
userSchema.virtual('fullname').get(function () {
    return this.firstname + ' ' + this.lastname;
});
//-----------------------------------------------------------------------------------------------------
userSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};
//-----------------------------------------------------------------------------------------------------
userSchema.methods.incLoginAttempts = function(cb) {
    // if we have a previous lock that has expired, restart at 1
    if (this.lockUntil && this.lockUntil < Date.now()) {
        return this.update({
            $set: { loginAttempts: 1 },
            $unset: { lockUntil: 1 }
        }, cb);
    }
    // otherwise we're incrementing
    var updates = { $inc: { loginAttempts: 1 } };
    // lock the account if we've reached max attempts and it's not locked already
    if (this.loginAttempts + 1 >= cfg.max_login_attempts && !this.isLocked) {
        updates.$set = { lockUntil: Date.now() + cfg.account_lock_time };
    }
    return this.update(updates, cb);
};
//-----------------------------------------------------------------------------------------------------
userSchema.methods.clearAfterLoginFields = function() {
    var obj = this.toObject();
    delete obj.__v;
    delete obj.password;
    delete obj.lockUntil;
    delete obj.loginAttempts;
    return obj;
};
//-----------------------------------------------------------------------------------------------------
// expose enum on the model, and provide an internal convenience reference
var reasons = userSchema.statics.failedLogin = {
    NOT_FOUND: 0,
    PASSWORD_INCORRECT: 1,
    MAX_ATTEMPTS: 2,
    ACTIVE: 3
};
//-----------------------------------------------------------------------------------------------------
userSchema.statics.getAuthenticated = function(username, password, req, cb) {
    this.findOne({ username: username })
        .populate('createdByUser', '-_id username firstname lastname')
        .populate('modifiedByUser', '-_id username firstname lastname')
        .populate('createdByRole', '-_id name title')
        .populate('modifiedByRole', '-_id name title')
        .populate('role', '-__v')
        .exec(function(err, user) {
        if (err) return cb(err);

        // make sure the user exists
        if (!user) {
            return cb(null, null, reasons.NOT_FOUND);
        }

        // check if the account is currently locked
        if (user.isLocked) {
            // just increment login attempts if account is already locked
            return user.incLoginAttempts(function(err) {
                if (err) return cb(err);
                return cb(null, null, reasons.MAX_ATTEMPTS);
            });
        }

        //test if account is disabled
        if(!user.active) {
            return cb(null, null, reasons.ACTIVE);
        }
        // test for a matching password
        user.comparePassword(password, function(err, isMatch) {

            if (err) return cb(err);
            // check if the password was a match
            if (isMatch) {
                // if there's no lock or failed attempts, just return the user
                if (!user.loginAttempts && !user.lockUntil) {
                    var updates = {};
                    if(user.currentLoginIp) {
                        updates = {
                            lastLoginIp: user.currentLoginIp,
                            currentLoginIp: utility.getRequestIp(req),
                            lastLoginDate: user.currentLoginDate,
                            currentLoginDate: Date.now()
                        };
                    }
                    else {
                        var date = Date.now();
                        updates = {
                            lastLoginIp: utility.getRequestIp(req),
                            currentLoginIp: utility.getRequestIp(req),
                            lastLoginDate: date,
                            currentLoginDate: date
                        };
                    }
                    console.log(updates);
                    return user.update(updates, function(err) {
                        if (err) return cb(err);
                        return cb(null, user);
                    });
                }
                // reset attempts and lock info
                var updates = {
                    $set: { loginAttempts: 0 },
                    $unset: { lockUntil: 1 },
                    $set: { lastLoginIp: user.currentLoginIp },
                    $set: { currentLoginIp: utility.getRequestIp(req) }
                };
                console.log(user.currentLoginIp);
                console.log(updates);
                return user.updateOne(updates, function(err) {
                    if (err) return cb(err);
                    return cb(null, user);
                });
            }
            // password is incorrect, so increment login attempts before responding
            user.incLoginAttempts(function(err) {
                if (err) return cb(err);
                return cb(null, null, reasons.PASSWORD_INCORRECT);
            });
        });
    });
};
//-----------------------------------------------------------------------------------------------------
module.exports = mongoose.model('User', userSchema, 'Users');
//-----------------------------------------------------------------------------------------------------