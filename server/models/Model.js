//-----------------------------------------------------------------------------------------------------
var async = require('async');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//-----------------------------------------------------------------------------------------------------
var modelSchema = mongoose.Schema({
    //-------------------------------------------------------------------------------------------------
    name: { type: String, lowercase: true, trim: true, required: 'Name is required', unique: true },
    realname: { type: String, required: 'Real name is required', unique: true },
    //-------------------------------------------------------------------------------------------------
    title : { type: String, required: 'Title is required' },
    subtitle : { type: String, required: 'Subtitle is required' },
    description : { type: String },
    //-------------------------------------------------------------------------------------------------
    permissions: [ new mongoose.Schema({
        role: { type: mongoose.Schema.ObjectId, ref: 'Role', required: 'Role is required' },

        view: { type: Boolean, default: true, required: 'View is required' },
        create: { type: Boolean, default: true, required: 'Create is required' },

        find_any: { type: Boolean, default: true, required: 'Find any is required' },
        update_any: { type: Boolean, default: true, required: 'Update any is required' },
        delete_any: { type: Boolean, default: true, required: 'Delete any is required' },

        find_role: { type: Boolean, default: true, required: 'Find role is required' },
        update_role: { type: Boolean, default: true, required: 'Update role is requried' },
        delete_role: { type: Boolean, default: true, required: 'Delete role is required' },

        find_own: { type: Boolean, default: true, required: 'Find own is required' },
        update_own: { type: Boolean, default: true, required: 'Update own is required' },
        delete_own: { type: Boolean, default: true, required: 'Delete own is required' }

    }, { _id: false }) ],
    //-------------------------------------------------------------------------------------------------
    route: { type: String, unique: true, required: 'Route is required'},
    //-------------------------------------------------------------------------------------------------
    createdAt: { type: Date, default: Date.now },
    modifiedAt: { type: Date, default: Date.now },
    //-------------------------------------------------------------------------------------------------
    createdByUser: { type: mongoose.Schema.ObjectId, ref: 'User' },
    modifiedByUser: { type: mongoose.Schema.ObjectId, ref: 'User' },
    //-------------------------------------------------------------------------------------------------
    createdByRole: { type: mongoose.Schema.ObjectId, ref: 'Role' },
    modifiedByRole: { type: mongoose.Schema.ObjectId, ref: 'Role' }
    //-------------------------------------------------------------------------------------------------
});
//-----------------------------------------------------------------------------------------------------
modelSchema.pre('save', function(next) {
    var obj = this;
    obj.modifiedAt = Date.now();
    next();
});
modelSchema.methods.clearAfterLoginFields = function() {
    var obj = this.toObject();
    delete obj.__v;
    return obj;
};
//-----------------------------------------------------------------------------------------------------
module.exports = mongoose.model('Model', modelSchema, 'Models');
//-----------------------------------------------------------------------------------------------------