//-----------------------------------------------------------------------------------------------------
var mongoose = require('mongoose');
//-----------------------------------------------------------------------------------------------------
var companySchema = mongoose.Schema({
    //-------------------------------------------------------------------------------------------------
    Domain: { type: String, trim: true, required: 'Domain is required', unique: true },
    CompanyName: { type: String, trim: true, index: true },
    Industry: { type: String, trim: true },
    Location: { type: String, trim: true },
    Description: { type: String, trim: true },
    WebSite: { type: String, trim: true },
    NoOfEmployees: { type: String, trim: true },
    Specialities: { type: String, trim: true },
    FoundedDate: { type: String, trim: true },
    SalesNavigatorLink: { type: String, trim: true },
    LinkedInUrl: { type: String, trim: true },
    UrlFromAnotherWebSite: { type: String, trim: true },
    IndustryTag: { type: String, trim: true },
    NoOfFounders: { type: String, trim: true },
    Founders: { type: String, trim: true },
    Revenue: { type: String, trim: true },
    Country: { type: String, trim: true },
    BillingStateProvince: { type: String, trim: true },
    BillingCity: { type: String, trim: true },
    BillingStreet: { type: String, trim: true },
    BillingZipPostalCode: { type: String, trim: true },
    OtherLocations: { type: String, trim: true },
    InfoEmail: { type: String, trim: true, index: true },
    AccountPhone: { type: String, trim: true },
    Products: { type: String, trim: true },
    Technologies: { type: String, trim: true },
    InvestorType: { type: String, trim: true },
    InvestmentStage: { type: String, trim: true },
    AmountOfInvestedMoney: { type: String, trim: true },
    //-------------------------------------------------------------------------------------------------
    createdAt: { type: Date, default: Date.now },
    modifiedAt: { type: Date, default: Date.now },
    //-------------------------------------------------------------------------------------------------
    //createdByUser: { type: mongoose.Schema.ObjectId, ref: 'User' },
    //modifiedByUser: { type: mongoose.Schema.ObjectId, ref: 'User' },
    //-------------------------------------------------------------------------------------------------
    //createdByRole: { type: mongoose.Schema.ObjectId, ref: 'Role' },
    //modifiedByRole: { type: mongoose.Schema.ObjectId, ref: 'Role' }
    //-------------------------------------------------------------------------------------------------
});
//-----------------------------------------------------------------------------------------------------
companySchema.pre('save', function (next) {
    var obj = this;
    obj.modifiedAt = Date.now();
    next();
});
//-----------------------------------------------------------------------------------------------------
companySchema.methods.clearFields = function () {
    var obj = this.toObject();
    if (obj.createdByUser) {
        var tmp = obj.createdByUser;
        delete obj.createdByUser;
        obj.createdByUser = {
            username: tmp.username,
            firstname: tmp.firstname,
            lastname: tmp.lastname
        };
    }
    if (obj.modifiedByUser) {
        var tmp = obj.modifiedByUser;
        delete obj.modifiedByUser;
        obj.modifiedByUser = {
            username: tmp.username,
            firstname: tmp.firstname,
            lastname: tmp.lastname
        };
    }
    if (obj.createdByRole) {
        var tmp = obj.createdByRole;
        delete obj.createdByRole;
        obj.createdByRole = {
            name: tmp.name,
            title: tmp.title
        };
    }
    if (obj.modifiedByRole) {
        var tmp = obj.modifiedByRole;
        delete obj.modifiedByRole;
        obj.modifiedByRole = {
            name: tmp.name,
            title: tmp.title
        };
    }
    delete obj.__v;
    return obj;
};
//-----------------------------------------------------------------------------------------------------
module.exports = mongoose.model('Company', companySchema, 'Companies');
//-----------------------------------------------------------------------------------------------------