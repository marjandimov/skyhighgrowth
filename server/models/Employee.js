//-----------------------------------------------------------------------------------------------------
var mongoose = require('mongoose');
//-----------------------------------------------------------------------------------------------------
var employeeSchema = mongoose.Schema({
    //-------------------------------------------------------------------------------------------------
    Domain: { type: String, trim: true, required: 'Domain is required', index: true },
    CompanyName: { type: String, trim: true },
    WebSite: { type: String, trim: true },
    FirstName: { type: String, trim: true },
    LastName: { type: String, trim: true },
    Title: { type: String, trim: true },
    Gender: { type: String, trim: true },
    MrMrs: { type: String, trim: true },
    Location: { type: String, trim: true },
    Email: { type: String, trim: true, required: 'Email is required', index: true },
    Phone: { type: String, trim: true },
    LinkedInProfileUrl: { type: String, trim: true },
    SalesNavigatorProfileUrl: { type: String, trim: true },
    UrlFromAnotherWebSite: { type: String, trim: true },
    Colleague: { type: String, trim: true },
    Vocativ: { type: String, trim: true },
    ShortBiography: { type: String, trim: true },
    Education: { type: String, trim: true },
    //-------------------------------------------------------------------------------------------------
    createdAt: { type: Date, default: Date.now },
    modifiedAt: { type: Date, default: Date.now },
    //-------------------------------------------------------------------------------------------------
    //createdByUser: { type: mongoose.Schema.ObjectId, ref: 'User' },
    //modifiedByUser: { type: mongoose.Schema.ObjectId, ref: 'User' },
    //-------------------------------------------------------------------------------------------------
    //createdByRole: { type: mongoose.Schema.ObjectId, ref: 'Role' },
    //modifiedByRole: { type: mongoose.Schema.ObjectId, ref: 'Role' }
    //-------------------------------------------------------------------------------------------------
});
//-----------------------------------------------------------------------------------------------------
employeeSchema.pre('save', function (next) {
    var obj = this;
    obj.modifiedAt = Date.now();
    next();
});
//-----------------------------------------------------------------------------------------------------
employeeSchema.methods.clearFields = function () {
    var obj = this.toObject();
    if (obj.createdByUser) {
        var tmp = obj.createdByUser;
        delete obj.createdByUser;
        obj.createdByUser = {
            username: tmp.username,
            firstname: tmp.firstname,
            lastname: tmp.lastname
        };
    }
    if (obj.modifiedByUser) {
        var tmp = obj.modifiedByUser;
        delete obj.modifiedByUser;
        obj.modifiedByUser = {
            username: tmp.username,
            firstname: tmp.firstname,
            lastname: tmp.lastname
        };
    }
    if (obj.createdByRole) {
        var tmp = obj.createdByRole;
        delete obj.createdByRole;
        obj.createdByRole = {
            name: tmp.name,
            title: tmp.title
        };
    }
    if (obj.modifiedByRole) {
        var tmp = obj.modifiedByRole;
        delete obj.modifiedByRole;
        obj.modifiedByRole = {
            name: tmp.name,
            title: tmp.title
        };
    }
    delete obj.__v;
    return obj;
};
//-----------------------------------------------------------------------------------------------------
module.exports = mongoose.model('Employee', employeeSchema, 'Employees');
//-----------------------------------------------------------------------------------------------------