//-----------------------------------------------------------------------------------------------------
var mongoose = require('mongoose');
//-----------------------------------------------------------------------------------------------------
var verifyEmailSchema = mongoose.Schema({
    //-------------------------------------------------------------------------------------------------
    firstname: { type: String, trim: true, required: 'Firstname is required' },
    lastname: { type: String, trim: true, required: 'Lastname is required' },
    domain: { type: String, trim: true, required: 'Domain is required' },
    email: { type: String, trim: true, lowercase: true, unique: true },
    valid: { type: Boolean },
    //-------------------------------------------------------------------------------------------------
    createdAt: { type: Date, default: Date.now },
    modifiedAt: { type: Date, default: Date.now },
    //-------------------------------------------------------------------------------------------------
    createdByUser: { type: mongoose.Schema.ObjectId, ref: 'User' },
    modifiedByUser: { type: mongoose.Schema.ObjectId, ref: 'User' },
    //-------------------------------------------------------------------------------------------------
    createdByRole: { type: mongoose.Schema.ObjectId, ref: 'Role' },
    modifiedByRole: { type: mongoose.Schema.ObjectId, ref: 'Role' }
    //-------------------------------------------------------------------------------------------------
});
//-----------------------------------------------------------------------------------------------------
verifyEmailSchema.pre('save', function (next) {
    var obj = this;
    obj.modifiedAt = Date.now();
    next();
});
//-----------------------------------------------------------------------------------------------------
verifyEmailSchema.methods.clearFields = function () {
    var obj = this.toObject();
    if (obj.createdByUser) {
        var tmp = obj.createdByUser;
        delete obj.createdByUser;
        obj.createdByUser = {
            username: tmp.username,
            firstname: tmp.firstname,
            lastname: tmp.lastname
        };
    }
    if (obj.modifiedByUser) {
        var tmp = obj.modifiedByUser;
        delete obj.modifiedByUser;
        obj.modifiedByUser = {
            username: tmp.username,
            firstname: tmp.firstname,
            lastname: tmp.lastname
        };
    }
    if (obj.createdByRole) {
        var tmp = obj.createdByRole;
        delete obj.createdByRole;
        obj.createdByRole = {
            name: tmp.name,
            title: tmp.title
        };
    }
    if (obj.modifiedByRole) {
        var tmp = obj.modifiedByRole;
        delete obj.modifiedByRole;
        obj.modifiedByRole = {
            name: tmp.name,
            title: tmp.title
        };
    }
    delete obj.__v;
    return obj;
};
//-----------------------------------------------------------------------------------------------------
module.exports = mongoose.model('VerifyEmail', verifyEmailSchema, 'VerifyEmails');
//-----------------------------------------------------------------------------------------------------