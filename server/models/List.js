//-----------------------------------------------------------------------------------------------------
var mongoose = require('mongoose');
//-----------------------------------------------------------------------------------------------------
var listSchema = mongoose.Schema({
    //-------------------------------------------------------------------------------------------------
    name: { type: String, trim: true, required: 'Name is required', unique: true },
    description: { type: String },
    //-------------------------------------------------------------------------------------------------
    group: { type: mongoose.Schema.ObjectId, ref: 'Group' },
    file: { type: String, trim: true, required: 'File is required' },
    sample: { type: String, trim: true, required: 'Sample is required' },
    price: { type: Number, required: 'Price is required' },
    //-------------------------------------------------------------------------------------------------
    createdAt: { type: Date, default: Date.now },
    modifiedAt: { type: Date, default: Date.now },
    //-------------------------------------------------------------------------------------------------
    createdByUser: { type: mongoose.Schema.ObjectId, ref: 'User' },
    modifiedByUser: { type: mongoose.Schema.ObjectId, ref: 'User' },
    //-------------------------------------------------------------------------------------------------
    createdByRole: { type: mongoose.Schema.ObjectId, ref: 'Role' },
    modifiedByRole: { type: mongoose.Schema.ObjectId, ref: 'Role' }
    //-------------------------------------------------------------------------------------------------
});
//-----------------------------------------------------------------------------------------------------
listSchema.pre('save', function (next) {
    var obj = this;
    obj.modifiedAt = Date.now();
    next();
});
//-----------------------------------------------------------------------------------------------------
listSchema.methods.clearFields = function () {
    var obj = this.toObject();
    if (obj.createdByUser) {
        var tmp = obj.createdByUser;
        delete obj.createdByUser;
        obj.createdByUser = {
            username: tmp.username,
            firstname: tmp.firstname,
            lastname: tmp.lastname
        };
    }
    if (obj.modifiedByUser) {
        var tmp = obj.modifiedByUser;
        delete obj.modifiedByUser;
        obj.modifiedByUser = {
            username: tmp.username,
            firstname: tmp.firstname,
            lastname: tmp.lastname
        };
    }
    if (obj.createdByRole) {
        var tmp = obj.createdByRole;
        delete obj.createdByRole;
        obj.createdByRole = {
            name: tmp.name,
            title: tmp.title
        };
    }
    if (obj.modifiedByRole) {
        var tmp = obj.modifiedByRole;
        delete obj.modifiedByRole;
        obj.modifiedByRole = {
            name: tmp.name,
            title: tmp.title
        };
    }
    delete obj.__v;
    return obj;
};
//-----------------------------------------------------------------------------------------------------
module.exports = mongoose.model('List', listSchema, 'Lists');
//-----------------------------------------------------------------------------------------------------