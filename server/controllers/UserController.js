//-----------------------------------------------------------------------------------------------------
var mongoose = require('mongoose');
var _ = require('lodash');
var utility = require('../utilities/utility');
var response = require('../utilities/response');
//var async = require('async');
var jwt = require('jsonwebtoken');
var cfg = require('../config/serverCfg');
//-----------------------------------------------------------------------------------------------------
var ModelObj = mongoose.model('User');
var Model = mongoose.model('Model');
//-----------------------------------------------------------------------------------------------------
module.exports = function(server) {
    var ModelCtrl = require('../controllers/ModelController')(server);
    return {
        //---------------------------------------------------------------------------------------------
        contactUs: function (req, res, next) {
            var mailOptions = {
                from: 'SkyHighGrowth <skyhighgrowthdb@gmail.com>', // sender address
                to: 'marjan.dimov@outlook.com', // list of receivers
                subject: 'Sky High Growth Contact Us', // Subject line
                text: 'First name: ' + req.body.firstname + '\n'
                    + 'Last name: ' + req.body.lastname + '\n'
                    + 'Phone: ' + req.body.phone + '\n'
                    + 'Email: ' + req.body.email + '\n'
                    + 'Company name: ' + req.body.companyname + '\n'
                , // plaintext body
                html: 'First name: ' + req.body.firstname + '<br />'
                    + 'Last name: ' + req.body.lastname + '<br />'
                    + 'Phone: ' + req.body.phone + '<br />'
                    + 'Email: ' + req.body.email + '<br />'
                    + 'Company name: ' + req.body.companyname + '<br />' // html body
            };
            utility.getTransporter().sendMail(mailOptions, function (err, info) {
                if (err) { return response.error(res, 500, {}, err); }
                else {
                    return response.ok(res, 200, [], 'Thank you for contacting us.');
                }
            });
        },
        forgotpassword: function (req, res, next) {
            ModelObj.findOne({ email: req.body.email }).exec(function(err, user) {
                if (err) { return response.error(res, 500); }
                if (!user) { return response.error(res, 404, {}, 'Email does not exists in our system'); }
                var pass = utility.generatePassword(8);
                return user.updateOne({
                    password: utility.generateHash(pass)
                }, function(err) {
                    if (err) return cb(err);
                    var mailOptions = {
                        from: 'SkyHighGrowth <skyhighgrowthdb@gmail.com>', // sender address
                        to: user.email, // list of receivers
                        subject: 'Sky High Growth Password Retrival', // Subject line
                        text: 'Your new credentials:\n username: ' + user.username + '\n password: ' + pass, // plaintext body
                        html: '<h3>Your new credentials:</h3><br/><b>username: </b>' + user.username + '<br/><b>password: </b>' + pass // html body
                    };
                    utility.getTransporter().sendMail(mailOptions, function(error, info) {
                        if (error) { return response.error(res, 500); }
                        else {
                            return response.ok(res, 200, data, 'Your new credentials are successfully sent to your email');
                        }
                    });
                });

            });
        },
        register: function (req, res, next) {
            if (cfg.disable_registraion) {
                return response.error(res, 404,[], 'Registration is currently disabled.');
            }
            ModelObj.findOne({ email: req.body.email }).exec(function (err, user) {
                if (err) { return response.error(res, 500); }
                if (user) { return response.error(res, 500, {}, 'Email already exist'); }
                ModelObj.findOne({ username: req.body.username }).exec(function(err, user) {
                    if (err) {
                        if (err) { return response.error(res, 500); }
                    }
                    if (user) { return response.error(res, 500, {}, 'Username already exist'); }
                    var Role =  mongoose.model('Role');
                    ModelObj.findOne({ username: 'system' }).exec(function(err, system) {
                        if (err) { return response.error(res, 500); }
                        if (!system) { return response.error(res, 500); }
                        Role.findOne({ name: 'member' }).exec(function(err, role) {
                            if (err) { return response.error(res, 500); }
                            if (!role) { return response.error(res, 404); }
                            req.body.active = true;
                            req.body.role = role._id;
                            req.body.gender = req.body.gender;
                            req.body.createdByUser =  system._id;
                            req.body.modifiedByUser =  system._id;
                            req.body.createdByRole =  role._id;
                            req.body.modifiedByRole =  role._id;

                            ModelObj.create(req.body, function (err, data) {
                                if (err) { return response.error(res, 500);}
                                if (!data) { return response.error(res, 404);}
                                return response.ok(res, 200, data, 'Registration succeeded');
                            });
                        });

                    });
                });

            });
        },
        login: function (req, res, next) {
            ModelObj.getAuthenticated(req.body.username, req.body.password, req, function(err, user, reason) {
                if (err) { return response.error(res, 500); }
                if (user) {
                    // handle login success
                    user.models = {};
                    var profile = {
                        username: user.username,
                        firstname: user.firstname,
                        lastname: user.lastname,
                        email: user.email,
                        phone: user.phone,
                        uid: user._id,
                        rid: user.role._id,
                        rname: user.role.name,
                        reqip: utility.getRequestIp(req)
                    };
                    var options = { expiresIn: cfg.token_expiration };
                    var token = jwt.sign(profile, cfg.secret_key, options);

                    ModelCtrl.findAllByRoleId(req, res, user.role._id, function (models) {
                        return res.status(200).jsonp({
                            data: user.clearAfterLoginFields(),
                            models: _.sortBy(models, function (o) { return o.order; }),
                            token: token,
                            status: {
                                code: 200,
                                message: 'Authentication succeeded',
                                messages: []
                            }
                        });

                        //return response.ok(res, 200, {}, 'Authentication succeeded');
                    });
                }
                var reasons = ModelObj.failedLogin;
                switch (reason) {
                    case reasons.NOT_FOUND:
                        return response.error(res, 404, {}, 'User not found');
                        break;
                    case reasons.PASSWORD_INCORRECT:
                        // note: these cases are usually treated the same - don't tell
                        // the user *why* the login failed, only that it did
                        utility.logErrorToConsole('PASSWORD_INCORRECT');
                        return response.error(res, 404, {}, 'Authentication failed');
                        break;
                    case reasons.MAX_ATTEMPTS:
                        // send email (nodemailer) or otherwise notify user that account is
                        // temporarily locked
                        /*
                         * Sorry, there have been more than 5 failed login attempts for this account.
                         * It is temporarily locked. Try again in 2 hours or request a new password.
                         * */
                        utility.logErrorToConsole('MAX_ATTEMPTS');
                        return response.error(res, 403, {}, 'Your account is temporarily locked for security reasons');
                        break;
                    case reasons.ACTIVE:
                        utility.logErrorToConsole('ACTIVE');
                        return response.error(res, 404, {}, 'Your account is permanently disabled');
                        break;
                }
            });
        },
        getLoggedUser: function (req, res, next) {
            var token = utility.extractTokenFromHeader(req);
            ModelObj.findOne({ _id: jwt.decode(token).uid })
                .populate('role', '-__v')
                .populate('createdByUser', '-_id username firstname lastname')
                .populate('modifiedByUser', '-_id username firstname lastname')
                .populate('createdByRole', '-_id name title')
                .populate('modifiedByRole', '-_id name title')
                .exec(function(err, data) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200, data);
            });
        },
        //---------------------------------------------------------------------------------------------
        findAll: function (req, res, next) {
            var query  = ModelObj.find({}, '-__v -password').sort('order');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('username').nin(['system']);
            query.populate('createdByUser', '-_id username firstname lastname')
                .populate('modifiedByUser', '-_id username firstname lastname')
                .populate('createdByRole', '-_id name title')
                .populate('modifiedByRole', '-_id name title')
                .populate('role', '-createdByUser -createdByRole -modifiedByUser -modifiedByRole')
                .exec(function (err, data) {
                    if (err) { return response.error(res, 500); }
                    if (!data) { return response.error(res, 404); }
                    return response.ok(res, 200, data);
                });
        },
        findOwnAccount: function (req, res, next) {
            console.log('findOwnAccount');
            console.log(req.options.id);
            var query = ModelObj.findOne({ _id: req.options.id }, '-__v -loginAttempts -role -password -active');
            query.where('username').ne('system');
            query.populate('createdByUser', '-__v -_id')
                .populate('modifiedByUser', '-__v -_id')
                .populate('createdByRole', '-__v -_id')
                .populate('modifiedByRole', '-__v -_id')
                .populate('role', '-__v -createdByUser -createdByRole -modifiedByUser -modifiedByRole')
                .exec(function (err, data) {
                    if (err) {
                        return response.error(res, 500);
                    }
                    console.log('data');
                    console.log(data)
                    return response.ok(res, 200, data);
                });
        },
        updateOwnAccount: function (req, res, next) {
            var query = ModelObj.findOne({ _id: req.options.id }, '_id');
            query.where('username').ne('system');
            query.exec(function (err, data) {
                if (err) { return response.error(res, 500); }
                if (!data) { return response.error(res, 404); }
                req.body.rid = req.options.rid;
                data.set({
                    firstname: req.body.firstname,
                    lastname: req.body.lastname,
                    phone: req.body.phone,
                    gender: req.body.gender,
                    companyname: req.body.companyname,
                    password: req.body.password

                });
                data.save(function (err) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200, data);
                });
            });
        },
        findOne: function (req, res, next) {
            console.log(req.options.id);
            var query  = ModelObj.findOne({ _id: req.options.id }, '-__v -loginAttempts');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('username').ne('system');
            query.populate('createdByUser', '-__v -_id')
                .populate('modifiedByUser', '-__v -_id')
                .populate('createdByRole', '-__v -_id')
                .populate('modifiedByRole', '-__v -_id')
                .populate('role', '-__v -createdByUser -createdByRole -modifiedByUser -modifiedByRole')
                .exec(function (err, data) {
                    if (err) {
                        return response.error(res, 500);
                    }
                    return response.ok(res, 200, data);
                });
        },
        create: function (req, res, next) {
            ModelObj.findOne({ email: req.body.email }).exec(function (err, user) {
                if (err) { return response.error(res, 500); }
                if (user) { return response.error(res, 500, {}, 'Email already exist'); }
                ModelObj.findOne({ username: req.body.username }).exec(function (err, user) {
                    if (err) {
                        if (err) { return response.error(res, 500); }
                    }
                    if (user) { return response.error(res, 500, {}, 'Username already exist'); }
                    ModelObj.create(req.body, function (err, data) {
                        if (err) { return response.error(res, 500); }
                        if (!data) { return response.error(res, 404); }
                        return response.ok(res, 200, data);
                    });
                });

            });
        },
        update: function (req, res, next) {
            var query  = ModelObj.findOne({ _id: req.options.id }, '_id');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('username').ne('system');
            query.exec(function (err, data) {
                if (err) { return response.error(res, 500); }
                if (!data) { return response.error(res, 404); }
                data.set(req.body);
                data.save(function (err) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200, data);
                });
            });
        },
        delete: function (req, res, next) {
            var query  = ModelObj.findOne({ _id: req.options.id }, '_id');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('username').ne('system');
            query.exec(function (err, data) {
                if (err) { return response.error(res, 500); }
                if (!data) { return response.error(res, 404); }
                data.remove(function (err) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200, data);
                });
            });
        }
        //---------------------------------------------------------------------------------------------
    }
};
//-----------------------------------------------------------------------------------------------------

