//-----------------------------------------------------------------------------------------------------
var mongoose = require('mongoose');
var _ = require('lodash');
var utility = require('../utilities/utility');
var response = require('../utilities/response');
var jwt = require('jsonwebtoken');
var cfg = require('../config/serverCfg.js');
var fs = require('fs');
//-----------------------------------------------------------------------------------------------------
var ModelObj = mongoose.model('Group');
//-----------------------------------------------------------------------------------------------------
module.exports = function(server) {
    var ModelCtrl = require('../controllers/ModelController')(server);
    return {
        //---------------------------------------------------------------------------------------------
        findAll: function (req, res) {
            ModelObj.countDocuments({}, function (err, cnt) {
                if (err) {
                    return response.error(res, 500);
                }
                req.options.count = cnt;
                var query = ModelObj.find({}, '-__v', { skip: req.options.skip, limit: req.options.limit });

                for (var i = 0; i < req.options.filters.length; i++) {
                    query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
                }
                query.populate('createdByUser', '-__v -_id')
                    .populate('modifiedByUser', '-__v -_id')
                    .populate('createdByRole', '-__v -_id')
                    .populate('modifiedByRole', '-__v -_id')
                    .populate('lists', '-__v -_id')
                    .exec(function (err, data) {
                        if (err) { return response.error(res, 500); }
                        return response.ok(res, 200, data);
                    });
            });

        },
        findOne: function (req, res) {
            var query = ModelObj.findOne({ _id: req.options.id }, '-__v');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.populate('createdByUser', '-__v -_id')
                .populate('modifiedByUser', '-__v -_id')
                .populate('createdByRole', '-__v -_id')
                .populate('modifiedByRole', '-__v -_id')
                .exec(function (err, data) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200, data);
                });
        },
        create: function (req, res) {
            ModelObj.create(req.body, function (err, data) {
                if (err) { return response.error(res, 500); }
                return response.ok(res, 200, data);
            });
        },
        update: function (req, res) {
            var query = ModelObj.findOne({ _id: req.options.id }, '_id');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.exec(function (err, data) {
                if (err) { return response.error(res, 500); }
                data.set(req.body);
                data.save(function (err) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200, data);
                });
            });
        },
        delete: function (req, res) {
            var query = ModelObj.findOne({ _id: req.options.id }, '_id');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.exec(function (err, data) {
                if (err) { return response.error(res, 500);}
                data.remove(function (err) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200);
                });
            });
        },
        count: function (req, res) {
            ModelObj.countDocuments({}, function (err, cnt) {
                if (err) {
                    return response.error(res, 500);
                }
                return cnt;
            });

        }
        //---------------------------------------------------------------------------------------------
    }
};
//-----------------------------------------------------------------------------------------------------

