//-----------------------------------------------------------------------------------------------------
var mongoose = require('mongoose');
var _ = require('lodash');
var utility = require('../utilities/utility');
var response = require('../utilities/response');
var jwt = require('jsonwebtoken');
var cfg = require('../config/serverCfg.js');
var fs = require('fs');
var async = require('async');
var json2csvParser = require('json2csv').Parser;
var csv = require('fast-csv');
var uuidv4 = require('uuid/v4');
//-----------------------------------------------------------------------------------------------------
var CompanyModel = mongoose.model('Company');
var EmployeeModel = mongoose.model('Employee');
//-----------------------------------------------------------------------------------------------------
module.exports = function(server) {
    return {
        //---------------------------------------------------------------------------------------------
        exportSelected: function (req, res) {
            var exportType = req.body.ExportType;
            var ids = req.body.ids;

            var filter = [];
            _.each(ids, function (item) {
                filter.push(mongoose.Types.ObjectId(item._id))
            });

            if (exportType === 'Companies') {
                var fields = [
                    { label: 'Domain', value: 'Domain' },
                    { label: 'Account Name', value: 'AccountName' },
                    { label: 'Industry', value: 'Industry' },
                    { label: 'Location', value: 'Location' },
                    { label: 'Description', value: 'Description' },
                    { label: 'Web Site', value: 'Web Site' },
                    { label: 'No Of Employees', value: 'NoOfEmployees' },
                    { label: 'Specialities', value: 'Specialities' },
                    { label: 'Founded Date', value: 'FoundedDate' },
                    { label: 'Linked In Url', value: 'LinkedInUrl' },
                    { label: 'Sales Navigator Link', value: 'SalesNavigatorLink' },
                    { label: 'Url From Another Site', value: 'UrlFromAnotherSite' },
                    { label: 'Industry Tag', value: 'IndustryTag' },
                    { label: 'No Of Founders', value: 'NoOfFounders' },
                    { label: 'Founders', value: 'Founders' },
                    { label: 'Revenue', value: 'Revenue' },
                    { label: 'Country', value: 'Country' },
                    { label: 'Billing State/Province', value: 'BillingStateProvince' },
                    { label: 'Billing City', value: 'BillingCity' },
                    { label: 'Billing Street', value: 'BillingStreet' },
                    { label: 'Billing Zip/Postal Code', value: 'BillingZipPostalCode' },
                    { label: 'Other Locations', value: 'OtherLocations' },
                    { label: 'Info Email', value: 'InfoEmail' },
                    { label: 'Account Phone', value: 'AccountPhone' },
                    { label: 'Products', value: 'Products' },
                    { label: 'Technologies', value: 'Technologies' },
                    { label: 'Inverstor Type', value: 'InverstorType' },
                    { label: 'Investment Stage', value: 'InvestmentStage' },
                    { label: 'Amount Of Invested Money', value: 'AmountOfInvestedMoney' },
                    { label: 'Raised Investor Type', value: 'RaisedInvestorType' },
                    { label: 'Raised Ivestment Stage', value: 'RaisedIvestmentStage' },
                    { label: 'Raised Amount Of Invested Money', value: 'RaisedAmountOfInvestedMoney' }
                ];
                var opts = { fields };

                CompanyModel.find({ '_id': { $in: filter } }, '-__v -createdAt -modifiedAt').lean().exec(function (err, data) {
                    if (err) { return response.error(res, 500); }

                    try {
                        var parser = new json2csvParser(opts);
                        var csv = parser.parse(data);
                        //var uid = uuidv4();
                        var dtStamp = utility.dateTimeStamp();
                        var path = './resources/files/tmp/' + req.options.uid + '_' + dtStamp + '_companies.csv';

                        fs.writeFile(path, csv, 'utf8', function (err) {
                            if (err) {
                                return response.error(res, 500);
                            }
                            else {
                                if (fs.existsSync(path)) {
                                    return res.download(path, function (err) {
                                        if (err) { return response.error(res, 500); }
                                    });
                                }
                                else {
                                    response.error(res, 404, 'There are no such file.');
                                }
                            }
                        });
                    }
                    catch (err) {
                        return response.error(res, 500);
                    }
                    
                });
            }
            else if (exportType === 'Employees') {
                var fields = [
                    { label: 'Domain', value: 'Domain' },
                    { label: 'Company Name', value: 'CompanyName' },
                    { label: 'Web Site', value: 'WebSite' },
                    { label: 'First Name', value: 'FirstName' },
                    { label: 'Last Name', value: 'LastName' },
                    { label: 'Title', value: 'Title' },
                    { label: 'Gender', value: 'Gender' },
                    { label: 'Mr/Mrs', value: 'MrMrs' },
                    { label: 'Location', value: 'Location' },
                    { label: 'Email', value: 'Email' },
                    { label: 'Phone', value: 'Phone' },
                    { label: 'Linked In Profile Url', value: 'LinkedInProfileUrl' },
                    { label: 'Sales Navigator Profile Url', value: 'SalesNavigatorProfileUrl' },
                    { label: 'Url From Another Web Site', value: 'UrlFromAnotherWebSite' },
                    { label: 'Colleague', value: 'Colleague' },
                    { label: 'Vocativ', value: 'Vocativ' },
                    { label: 'Short Biography', value: 'ShortBiography' },
                    { label: 'Education', value: 'Education' }
                ];
                var opts = { fields };

                EmployeeModel.find({ '_id': { $in: filter } }, '-__v -createdAt -modifiedAt').lean().exec(function (err, data) {
                    if (err) { return response.error(res, 500); }

                    try {
                        var parser = new json2csvParser(opts);
                        var csv = parser.parse(data);
                        //var uid = uuidv4();
                        var dtStamp = utility.dateTimeStamp();
                        var path = './resources/files/tmp/' + req.options.uid + '_' + dtStamp + '_employees.csv';

                        fs.writeFile(path, csv, 'utf8', function (err) {
                            if (err) {
                                return response.error(res, 500);
                            }
                            else {
                                if (fs.existsSync(path)) {
                                    return res.download(path, function (err) {
                                        if (err) { return response.error(res, 500); }
                                    });
                                }
                                else {
                                    response.error(res, 404, 'There are no such file.');
                                }
                            }
                        });
                    }
                    catch (err) {
                        return response.error(res, 500);
                    }
                });
            }

        },
        search: function (req, res) {
           
            var companies = [];
            var employees = [];
            var companiesFindFilter = [];
            var employeesFindFilter = [];

            var searchType = req.body.SearchType;

            // Companies Filters
            if (req.body.CompanyName) {
                companiesFindFilter.push({ $and: [{ 'CompanyName': { $regex: '.*' + req.body.CompanyName + '*.', '$options': 'i' } }] });
            }
            if (req.body.Industry) {
                if (req.body.Industry.length !== 0) {
                    companiesFindFilter.push({ $and: [{ 'Industry': { $in: req.body.Industry } }] });
                }
            }
            if (req.body.Location) {
                if (req.body.Location.length !== 0) {
                    companiesFindFilter.push({ $and: [{ 'Location': { $in: req.body.Location } }] });
                }
            }
            if (req.body.IndustryTag) {
                if (req.body.IndustryTag.length !== 0) {
                    companiesFindFilter.push({ $and: [{ 'IndustryTag': { $in: req.body.IndustryTag } }] });
                }
            }
            if (req.body.Country) {
                if (req.body.Country.length !== 0) {
                    companiesFindFilter.push({ $and: [{ 'Country': { $in: req.body.Country } }] });
                }
            }
            if (req.body.Specialities) {
                if (req.body.Specialities.length !== 0) {
                    companiesFindFilter.push({ $and: [{ 'Specialities': { $in: req.body.Specialities } }] });
                }
            }
            if (req.body.NoOfEmployees) {
                if (req.body.NoOfEmployees.length !== 0) {
                    companiesFindFilter.push({ $and: [{ 'NoOfEmployees': { $in: req.body.NoOfEmployees } }] });
                }
            }

            // Employees Filter
            if (req.body.Title) {
                employeesFindFilter.push({ $and: [{ 'Title': { $regex: '.*' + req.body.Title + '*.', '$options': 'i' } }] });
            }
            if (req.body.FirstName) {
                employeesFindFilter.push({ $and: [{ 'FirstName': { $regex: '.*' + req.body.FirstName + '*.', '$options': 'i' } }] });
            }
            if (req.body.LastName) {
                employeesFindFilter.push({ $and: [{ 'LastName': { $regex: '.*' + req.body.LastName + '*.', '$options': 'i' } }] });
            }
            if (req.body.Gender) {
                if (req.body.Gender.length !== 0) {
                    employeesFindFilter.push({ $and: [{ 'Gender': { $in: req.body.Gender } }] });
                }
            }

            if (companiesFindFilter.length === 0) {
                companiesFindFilter = {};
            }
            else {
                companiesFindFilter = { $and: companiesFindFilter };
            }
            CompanyModel.countDocuments(companiesFindFilter, function (err, cnt) {
                if (err) {
                    return response.error(res, 500);
                }
                CompanyModel.find(companiesFindFilter, '-__v', { skip: req.options.skip, limit: req.options.limit }).lean().exec(function (err, data) {
                    if (err) { return response.error(res, 500); }
                    companies = data;
                    var doms = _.uniq(_.map(data, function (el, idx) {
                        return el.Domain;
                    }));

                    employeesFindFilter.push({ $and: [{ 'Domain': { $in: doms } }] });
                    employeesFindFilter = { $and: employeesFindFilter };

                    EmployeeModel.find(employeesFindFilter, '-__v').lean().exec(function (err, data) {
                        if (err) { return response.error(res, 500); }
                        employees = data;

                        res.pagging.count = cnt;
                        res.pagging.pages = Math.ceil(cnt / req.options.limit);

                        if (searchType === 'Companies') {
                            _.each(companies, function (item, i) {
                                item.Employees = utility.filterBy(employees, 'Domain', item.Domain);
                            });

                            //masking
                            var keyC, keyE;
                            _.each(companies, function (c, i) {
                                for (keyC in c) {
                                    if (c.hasOwnProperty(keyC)) {
                                        if (keyC === 'Employees') {
                                            _.each(c.Employees, function (e, i) {
                                                for (keyE in e) {
                                                    if (e.hasOwnProperty(keyE)) {
                                                        if (e[keyE]) {
                                                            if (keyE !== '_id') {
                                                                e[keyE] = Array.from(e[keyE], e => Math.random() < 0.5 ? '*' : e).join('');
                                                            }
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                        else {
                                            if (c[keyC]) {
                                                if (keyC !== '_id') {
                                                    c[keyC] = Array.from(c[keyC], c => Math.random() < 0.5 ? '*' : c).join('');
                                                }
                                            }
                                        }
                                    }
                                }
                            });

                            return response.ok(res, 200, companies);
                        }
                        if (searchType === 'Employees') {
                            _.each(employees, function (item, i) {
                                item.Companies = utility.filterBy(companies, 'Domain', item.Domain);
                            });

                            //masking
                            var keyC, keyE;
                            _.each(employees, function (e, i) {
                                for (keyE in e) {
                                    if (e.hasOwnProperty(keyE)) {
                                        if (keyE === 'Companies') {
                                            _.each(e.Companies, function (c, i) {
                                                for (keyC in c) {
                                                    if (c.hasOwnProperty(keyC)) {
                                                        if (c[keyC]) {
                                                            if (keyC !== '_id') {
                                                                c[keyC] = Array.from(c[keyC], c => Math.random() < 0.5 ? '*' : c).join('');
                                                            }
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                        else {
                                            if (e[keyE]) {
                                                if (keyE !== '_id') {
                                                    e[keyE] = Array.from(e[keyE], e => Math.random() < 0.5 ? '*' : e).join('');
                                                }
                                            }
                                        }
                                    }
                                }
                            });

                            return response.ok(res, 200, employees);
                        }
                    });
                });
            });
        },
        filters: function (req, res) {
            var data = {
                industries: [],
                locations: [],
                industryTags: [],
                countries: [],
                specialities: [],
                numberOfEmployees: []
            }

            CompanyModel.find({ '$and': [{ 'Industry': { '$ne': null } }, { 'Industry': { '$ne': '/' } }] }).distinct('Industry', function (err, industries) {
                if (err) { return response.error(res, 500); }
                data.industries = industries;

                CompanyModel.find({ '$and': [{ 'Location': { '$ne': null } }, { 'Location': { '$ne': '/' } }] }).distinct('Location', function (err, locations) {
                    if (err) { return response.error(res, 500); }
                    data.locations = locations;

                    CompanyModel.find({ '$and': [{ 'IndustryTag': { '$ne': null } }, { 'IndustryTag': { '$ne': '/' } }] }).distinct('IndustryTag', function (err, industryTags) {
                        if (err) { return response.error(res, 500); }
                        data.industryTags = industryTags;

                        CompanyModel.find({ '$and': [{ 'Country': { '$ne': null } }, { 'Country': { '$ne': '/' } }] }).distinct('Country', function (err, countries) {
                            if (err) { return response.error(res, 500); }
                            data.countries = countries;

                            CompanyModel.find({ '$and': [{ 'Specialities': { '$ne': null } }, { 'Specialities': { '$ne': '/' } }] }).distinct('Specialities', function (err, specialities) {
                                if (err) { return response.error(res, 500); }
                                data.specialities = specialities;

                                CompanyModel.find({ '$and': [{ 'NoOfEmployees': { '$ne': null } }, { 'NoOfEmployees': { '$ne': '/' } }] }).distinct('NoOfEmployees', function (err, numberOfEmployees) {
                                    if (err) { return response.error(res, 500); }
                                    data.numberOfEmployees = numberOfEmployees;

                                    return response.ok(res, 200, data);
                                });
                            });
                        });
                    });
                });


               
            });
        },
        count: function (req, res) {
            ModelObj.countDocuments({}, function (err, cnt) {
                if (err) {
                    return response.error(res, 500);
                }
                return cnt;
            });

        }
        //---------------------------------------------------------------------------------------------
    }
};
//-----------------------------------------------------------------------------------------------------

