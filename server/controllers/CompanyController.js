//-----------------------------------------------------------------------------------------------------
var mongoose = require('mongoose');
var _ = require('lodash');
var utility = require('../utilities/utility');
var response = require('../utilities/response');
var jwt = require('jsonwebtoken');
var cfg = require('../config/serverCfg.js');
var fs = require('fs');
var async = require('async');

var csv = require('fast-csv');
//-----------------------------------------------------------------------------------------------------
var ModelObj = mongoose.model('Company');
//-----------------------------------------------------------------------------------------------------
module.exports = function(server) {
    return {
        //---------------------------------------------------------------------------------------------
        import: function (req, res) {
            var companies = [];
            var notValid = [];
            var domains = [];
            var headers = [
                'Domain',
                'CompanyName',
                'Industry',
                'Location',
                'Description',
                'WebSite',
                'NoOfEmployees',
                'Specialities',
                'FoundedDate',
                'SalesNavigatorLink',
                'LinkedInUrl',
                'UrlFromAnotherWebSite',
                'IndustryTag',
                'NoOfFounders',
                'Founders',
                'Revenue',
                'Country',
                'BillingStateProvince',
                'BillingCity',
                'BillingStreet',
                'BillingZipPostalCode',
                'OtherLocations',
                'InfoEmail',
                'AccountPhone',
                'Products',
                'Technologies',
                'InvestorType',
                'InvestmentStage',
                'AmountOfInvestedMoney'
            ];
            var cnt = 0;
            csv.fromPath('resources/files/EM_Other_Firme.csv', {
                headers: headers,
                ignoreEmpty: true,
                objectMode: true,
                renameHeaders: true
            })
            .on('data', function (data) {
                if (data.Domain) {
                    if (data.Domain === '') {
                        notValid.push(data);
                    }
                    else {
                        domains.push(data.Domain);
                       
                        companies.push(data);
                        if (cnt < 1) {
                            console.log(data);
                        }
                        cnt++;
                    }
                }
                else {
                    notValid.push(data);
                }

            })
            .on('end', function () {

                console.log('parse done');

                ModelObj.bulkWrite(
                    companies.map((data) =>
                        ({
                            updateOne: {
                                filter: { Domain: data.Domain },
                                update: {
                                    $set: {
                                        Domain: data.Domain,
                                        CompanyName: data.CompanyName,
                                        Industry: data.Industry,
                                        Location: data.Location,
                                        Description: data.Description,
                                        WebSite: data.WebSite,
                                        NoOfEmployees: data.NoOfEmployees,
                                        Specialities: data.Specialities,
                                        FoundedDate: data.FoundedDate,
                                        SalesNavigatorLink: data.SalesNavigatorLink,
                                        LinkedInUrl: data.LinkedInUrl,
                                        UrlFromAnotherWebSite: data.UrlFromAnotherWebSite,
                                        IndustryTag: data.IndustryTag,
                                        NoOfFounders: data.NoOfFounders,
                                        Founders: data.Founders,
                                        Revenue: data.Revenue,
                                        Country: data.Country,
                                        BillingStateProvince: data.BillingStateProvince,
                                        BillingCity: data.BillingCity,
                                        BillingStreet: data.BillingStreet,
                                        BillingZipPostalCode: data.BillingZipPostalCode,
                                        OtherLocations: data.OtherLocations,
                                        InfoEmail: data.InfoEmail,
                                        AccountPhone: data.AccountPhone,
                                        Products: data.Products,
                                        Technologies: data.Technologies,
                                        InvestorType: data.InvestorType,
                                        InvestmentStage: data.InvestmentStage,
                                        AmountOfInvestedMoney: data.AmountOfInvestedMoney
                                    }
                                },
                                upsert: true
                            }
                        })
                    )
                )
                .then(bulkWriteOpResult => {
                    console.log('import done');
                    res.pagging = {
                        limit: 0,
                        skip:0
                    };
                return response.ok(res, 200, bulkWriteOpResult);
                })
                .catch(err => {
                    console.log(err);
                    return response.error(res, 500);
                });
            });
        },
        //---------------------------------------------------------------------------------------------
        findAll: function (req, res) {
            ModelObj.countDocuments({}, function (err, cnt) {
                if (err) {
                    return response.error(res, 500);
                }
                res.pagging.count = cnt;
                var query = ModelObj.find({}, '-__v', { skip: req.options.skip, limit: req.options.limit });

                for (var i = 0; i < req.options.filters.length; i++) {
                    query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
                }
                query.populate('createdByUser', '-__v -_id')
                    .populate('modifiedByUser', '-__v -_id')
                    .populate('createdByRole', '-__v -_id')
                    .populate('modifiedByRole', '-__v -_id')
                    .populate('lists', '-__v -_id')
                    .exec(function (err, data) {
                        if (err) { console.log(err); return response.error(res, 500); }
                        return response.ok(res, 200, data);
                    });
            });

        },
        findOne: function (req, res) {
            var query = ModelObj.findOne({ _id: req.options.id }, '-__v');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.populate('createdByUser', '-__v -_id')
                .populate('modifiedByUser', '-__v -_id')
                .populate('createdByRole', '-__v -_id')
                .populate('modifiedByRole', '-__v -_id')
                .exec(function (err, data) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200, data);
                });
        },
        create: function (req, res) {
            ModelObj.create(req.body, function (err, data) {
                if (err) { return response.error(res, 500); }
                return response.ok(res, 200, data);
            });
        },
        update: function (req, res) {
            var query = ModelObj.findOne({ _id: req.options.id }, '_id');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.exec(function (err, data) {
                if (err) { return response.error(res, 500); }
                data.set(req.body);
                data.save(function (err) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200, data);
                });
            });
        },
        delete: function (req, res) {
            var query = ModelObj.findOne({ _id: req.options.id }, '_id');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.exec(function (err, data) {
                if (err) { return response.error(res, 500);}
                data.remove(function (err) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200);
                });
            });
        },
        count: function (req, res) {
            ModelObj.countDocuments({}, function (err, cnt) {
                if (err) {
                    return response.error(res, 500);
                }
                return cnt;
            });

        }
        //---------------------------------------------------------------------------------------------
    }
};
//-----------------------------------------------------------------------------------------------------

