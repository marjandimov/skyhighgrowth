//-----------------------------------------------------------------------------------------------------
var mongoose = require('mongoose');
var _ = require('lodash');
var utility = require('../utilities/utility');
var response = require('../utilities/response');
var jwt = require('jsonwebtoken');
var cfg = require('../config/serverCfg.js');
var fs = require('fs');
var csv = require('fast-csv');
//-----------------------------------------------------------------------------------------------------
var ModelObj = mongoose.model('Employee');
//-----------------------------------------------------------------------------------------------------
module.exports = function(server) {
    return {
        //---------------------------------------------------------------------------------------------
        import: function (req, res) {
            var employees = [];
            var notValid = [];
            var domains = [];
            var headers = [
                'Domain',
                'CompanyName',
                'WebSite',
                'FirstName',
                'LastName',
                'Title',
                'Gender',
                'MrMrs',
                'Location',
                'Email',
                'Phone',
                'LinkedInProfileUrl',
                'SalesNavigatorProfileUrl',
                'UrlFromAnotherWebSite',
                'Colleague',
                'Vocativ',
                'ShortBiography',
                'Education'
            ];
            var cnt = 0;
            csv.fromPath('resources/files/EM_Other_Ljudi.csv', {
                headers: headers,
                ignoreEmpty: true,
                objectMode: true,
                renameHeaders: true
            })
                .on('data', function (data) {
                    if (data.Domain) {
                        if (data.Domain === '') {
                            notValid.push(data);
                        }
                        else {
                            domains.push(data.Domain);

                            employees.push(data);
                            if (cnt < 1) {
                                console.log(data);
                            }
                            cnt++;
                        }
                    }
                    else {
                        notValid.push(data);
                    }

                })
                .on('end', function () {

                    console.log('parse done');
                    console.log('-------------------> starting bulkwrite')
                    ModelObj.bulkWrite(
                        employees.map((data) =>
                            ({
                                updateOne: {
                                    filter: { Email: data.Email },
                                    update: {
                                        $set: {
                                            // Employees: data.Employees,
                                            Domain: data.Domain,
                                            CompanyName: data.CompanyName,
                                            WebSite: data.WebSite,
                                            FirstName: data.FirstName,
                                            LastName: data.LastName,
                                            Title: data.Title,
                                            Gender: data.Gender,
                                            MrMrsdata: data.MrMrsdata,
                                            Location: data.Location,
                                            Email: data.Email,
                                            Phone: data.Phone,
                                            LinkedInProfileUrl: data.LinkedInProfileUrl,
                                            SalesNavigatorProfileUrl: data.SalesNavigatorProfileUrl,
                                            UrlFromAnotherWebSite: data.UrlFromAnotherWebSite,
                                            Colleague: data.Colleague,
                                            Vocativ: data.Vocativ,
                                            ShortBiography: data.ShortBiography,
                                            Education: data.Education
                                        }
                                    },
                                    upsert: true
                                }
                            })
                        )
                    )
                        .then(bulkWriteOpResult => {
                            console.log('import done');
                            res.pagging = {
                                limit: 0,
                                skip: 0
                            };
                            return response.ok(res, 200, bulkWriteOpResult);
                        })
                        .catch(err => {
                            console.log(err);
                            return response.error(res, 500);
                        });
                });
        },
        search: function () {
            var query = ModelObj.find({ _id: req.options.id }, '-__v');
        },
        //---------------------------------------------------------------------------------------------
        findAll: function (req, res) {
            ModelObj.countDocuments({}, function (err, cnt) {
                if (err) {
                    return response.error(res, 500);
                }
                res.pagging.count = cnt;
                var query = ModelObj.find({}, '-__v', { skip: req.options.skip, limit: req.options.limit });

                for (var i = 0; i < req.options.filters.length; i++) {
                    query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
                }
                query.populate('createdByUser', '-__v -_id')
                    .populate('modifiedByUser', '-__v -_id')
                    .populate('createdByRole', '-__v -_id')
                    .populate('modifiedByRole', '-__v -_id')
                    .populate('lists', '-__v -_id')
                    .exec(function (err, data) {
                        if (err) { console.log(err); return response.error(res, 500); }
                        return response.ok(res, 200, data);
                    });
            });

        },
        findOne: function (req, res) {
            var query = ModelObj.findOne({ _id: req.options.id }, '-__v');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.populate('createdByUser', '-__v -_id')
                .populate('modifiedByUser', '-__v -_id')
                .populate('createdByRole', '-__v -_id')
                .populate('modifiedByRole', '-__v -_id')
                .exec(function (err, data) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200, data);
                });
        },
        create: function (req, res) {
            ModelObj.create(req.body, function (err, data) {
                if (err) { return response.error(res, 500); }
                return response.ok(res, 200, data);
            });
        },
        update: function (req, res) {
            var query = ModelObj.findOne({ _id: req.options.id }, '_id');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.exec(function (err, data) {
                if (err) { return response.error(res, 500); }
                data.set(req.body);
                data.save(function (err) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200, data);
                });
            });
        },
        delete: function (req, res) {
            var query = ModelObj.findOne({ _id: req.options.id }, '_id');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.exec(function (err, data) {
                if (err) { return response.error(res, 500);}
                data.remove(function (err) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200);
                });
            });
        },
        count: function (req, res) {
            ModelObj.countDocuments({}, function (err, cnt) {
                if (err) {
                    return response.error(res, 500);
                }
                return cnt;
            });

        }
        //---------------------------------------------------------------------------------------------
    }
};
//-----------------------------------------------------------------------------------------------------

