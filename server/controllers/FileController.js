//-----------------------------------------------------------------------------------------------------
var mongoose = require('mongoose');
var _ = require('lodash');
var utility = require('../utilities/utility');
var response = require('../utilities/response');
//var async = require('async');
var jwt = require('jsonwebtoken');
var cfg = require('../config/serverCfg');
var fs = require('fs');
//-----------------------------------------------------------------------------------------------------
var ModelObj = mongoose.model('User');
var Model = mongoose.model('Model');
//-----------------------------------------------------------------------------------------------------
module.exports = function(server) {
    return {
        //---------------------------------------------------------------------------------------------
        findAll: function (req, res, next) {
            var files = utility.getFiles('./resources/files/');
            response.ok(res, 200, files);
        },
        create: function (req, res, next) {
            if (req.files) {
                if (_.indexOf(cfg.allowed_file_types.files, req.files.file.mimetype) === -1) {
                    return response.error(res, 500, {}, 'File type is not allowed for upload');
                }
                var path = './resources/files/' + req.files.file.name; // + req.body.name;
                req.files.file.mv(path, function (err) {
                    if (err) {
                        console.log(err);
                        return response.error(res, 500);
                    }
                    response.ok(res, 200, {}, 'File successfully upload into group ' + req.body.group);
                });
            }
            else {
                return response.error(res, 500, {}, 'No file to upload');
            }
        },
        delete: function (req, res, next) {
            console.log('usao di treba!');
            var path = './resources/files/' + req.params.name;
            console.log(path);
            try {
                fs.unlink(path, function (err) {
                    console.log(err);
                    if (err) { return response.error(res, 500); }
                    response.ok(res, 200, {});
                });  
                
            }
            catch (err) {
                console.log('catch')
                console.log(err);
                return response.error(res, 500);
            }
        }
        //---------------------------------------------------------------------------------------------
    }
};
//-----------------------------------------------------------------------------------------------------

