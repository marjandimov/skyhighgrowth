//-----------------------------------------------------------------------------------------------------
var mongoose = require('mongoose');
var _ = require('lodash');
var utility = require('../utilities/utility');
var response = require('../utilities/response');
var jwt = require('jsonwebtoken');
var cfg = require('../config/serverCfg.js');
var fs = require('fs');
var mime = require('mime');
//-----------------------------------------------------------------------------------------------------
var ModelObj = mongoose.model('List');
var ModelGroup = mongoose.model('Group');
//-----------------------------------------------------------------------------------------------------
module.exports = function (server) {
    var ModelCtrl = require('../controllers/ModelController')(server);
    return {
        getLists: function (req, res, next) {
            var token = utility.extractTokenFromHeader(req);

            ModelObj.find({}, '-__v -createdByUser -modifiedByUser -createdByRole -modifiedByRole -createdAt -modifiedAt')
                .populate('group', '-__v -createdByUser -modifiedByUser -createdByRole -modifiedByRole')
                //.populate('createdByUser', '-__v -_id')
                //.populate('modifiedByUser', '-__v -_id')
                //.populate('createdByRole', '-__v -_id')
                //.populate('modifiedByRole', '-__v -_id')
                .exec(function (err, data) {
                    if (err) { return response.error(res, 500); }

                    var arrResult = JSON.parse(JSON.stringify(data));
                   
                    var result = _.flow([
                        (arr) => _.groupBy(arr, 'group.name'),
                        (groups) => _.map(groups, (g) => ({
                            ..._.head(g).group,
                            lists: _.map(g, o => _.pick(o, ['_id', 'name', 'description', 'price', 'file', 'sample']))
                        }))
                    ])(arrResult);

                    for (var i = 0; i < result.length; i++) {
                        for (var ii = 0; ii < result[i].lists.length; ii++) {
                            if (fs.existsSync('./resources/files/' + result[i].lists[ii].file)) {
                                result[i].lists[ii].count = fs.readFileSync('./resources/files/' + result[i].lists[ii].file).toString().split('\n').length - 2;
                            }
                        }
                    }
                    return response.ok(res, 200, result);
                });
        },
        downloadSample: function (req, res) {
            console.log(req.body.id)
            ModelObj.findOne({ _id: req.body.id }, '-__v').exec(function (err, data) {
                if (err) { return response.error(res, 500); }
                console.log(data);

                if (fs.existsSync('./resources/files/' + data.file)) {
                    var path = './resources/files/' + data.file;

                    return res.download(path, function (err) {
                        if (err) { return response.error(res, 500); }
                    });
                }
                else {
                    response.error(res, 404, 'There are no such file.');
                }

                //return response.ok(res, 200, data);
            });
        },
        //---------------------------------------------------------------------------------------------
        findAll: function (req, res) {
            ModelObj.countDocuments({}, function (err, cnt) {
                if (err) {
                    return response.error(res, 500);
                }
                req.options.count = cnt;
                var query = ModelObj.find({}, '-__v', { skip: req.options.skip, limit: req.options.limit });

                for (var i = 0; i < req.options.filters.length; i++) {
                    query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
                }
                query.populate('createdByUser', '-__v -_id')
                    .populate('modifiedByUser', '-__v -_id')
                    .populate('createdByRole', '-__v -_id')
                    .populate('modifiedByRole', '-__v -_id')
                    .populate('group', '-__v')
                    .exec(function (err, data) {
                        if (err) { return response.error(res, 500); }
                        return response.ok(res, 200, data);
                    });
            });

        },
        findOne: function (req, res) {
            var query = ModelObj.findOne({ _id: req.options.id }, '-__v');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.populate('createdByUser', '-__v -_id')
                .populate('modifiedByUser', '-__v -_id')
                .populate('createdByRole', '-__v -_id')
                .populate('modifiedByRole', '-__v -_id')
                .populate('group', '-__v')
                .exec(function (err, data) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200, data);
                });
        },
        create: function (req, res) {
            ModelObj.create(req.body, function (err, data) {
                if (err) { return response.error(res, 500); }
                return response.ok(res, 200, data);
            });
        },
        update: function (req, res) {
            var query = ModelObj.findOne({ _id: req.options.id }, '_id');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.exec(function (err, data) {
                if (err) { return response.error(res, 500); }
                data.set(req.body);
                data.save(function (err) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200, data);
                });
            });
        },
        delete: function (req, res) {
            var query = ModelObj.findOne({ _id: req.options.id }, '_id');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.exec(function (err, data) {
                if (err) { return response.error(res, 500); }
                data.remove(function (err) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200);
                });
            });
        },
        count: function (req, res) {
            ModelObj.countDocuments({}, function (err, cnt) {
                if (err) {
                    return response.error(res, 500);
                }
                return cnt;
            });

        }
        //---------------------------------------------------------------------------------------------
    }
};
//-----------------------------------------------------------------------------------------------------

