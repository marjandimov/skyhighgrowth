//-----------------------------------------------------------------------------------------------------
var mongoose = require('mongoose');
var _ = require('lodash');
var utility = require('../utilities/utility');
var response = require('../utilities/response');
var async = require('async');
//-----------------------------------------------------------------------------------------------------
var ModelObj = mongoose.model('Role');
//-----------------------------------------------------------------------------------------------------
module.exports = function(server) {
    return {
        //---------------------------------------------------------------------------------------------
        findAll: function (req, res) {
            ModelObj.count({}, function (err, cnt) {
                if (err) {
                    return response.error(res, 500);
                }
                if (!cnt) {
                    return response.error(res, 404);
                }
                req.options.count = cnt;
                var query  = ModelObj.find({}, '-__v', { skip: req.options.skip, limit: req.options.limit });
                //console.log(req.options);

                for (var i = 0; i < req.options.filters.length; i++) {
                    query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
                }
                query.where('name').ne('system');
                query.populate('createdByUser', '-_id username firstname lastname')
                    .populate('modifiedByUser', '-_id username firstname lastname')
                    .populate('createdByRole', '-_id name title')
                    .populate('modifiedByRole', '-_id name title')
                    .exec(function (err, data) {
                        if (err) { return response.error(res, 500); }
                        if (!data) { return response.error(res, 404); }
                        return response.ok(res, 200, data);
                });
            });

        },
        findOne: function (req, res) {
            var query  = ModelObj.findOne({ _id: req.options.id }, '-__v');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.populate('createdByUser', '-_id username firstname lastname')
                .populate('modifiedByUser', '-_id username firstname lastname')
                .populate('createdByRole', '-_id name title')
                .populate('modifiedByRole', '-_id name title')
                .exec(function (err, data) {
                    if (err) { return response.error(res, 500); }
                    if (!data) { return response.error(res, 404); }
                    return response.ok(res, 200, data);
                });
        },
        create: function (req, res) {
            ModelObj.create(req.body, function (err, data) {
                if (err) { return response.error(res, 500); }
                if (!data) { return response.error(res, 404); }
                return response.ok(res, 200, data);
            });
        },
        update: function (req, res) {
            var query  = ModelObj.findOne({ _id: req.options.id }, '_id');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.exec(function (err, data) {
                if (err) { return response.error(res, 500); }
                if (!data) { return response.error(res, 404); }
                    data.set(req.body);
                    data.save(function (err) {
                        if (err) { return response.error(res, 500); }
                        return response.ok(res, 200, data);
                    });
            });
        },
        delete: function (req, res) {
            var query  = ModelObj.findOne({ _id: req.options.id }, '_id');
            for (var i = 0; i < req.options.filters.length; i++) {
                query.where(req.options.filters[i].fieldName).equals(req.options.filters[i].value);
            }
            query.where('name').ne('system');
            query.exec(function (err, data) {
                if (err) { return response.error(res, 500); }
                if (!data) { return response.error(res, 404); }
                data.remove(function (err) {
                    if (err) { return response.error(res, 500); }
                    return response.ok(res, 200, data);
                });
            });
        },
        count: function(req, res) {
            ModelObj.count({}, function (err, cnt) {
                if (err) {
                    return response.error(res, 500);
                }
                if (!cnt) {
                    return response.error(res, 404);
                }
                return cnt;
            });

        }
        //---------------------------------------------------------------------------------------------
    }
};
//-----------------------------------------------------------------------------------------------------

