//-----------------------------------------------------------------------------------------------------
var utility = require('../utilities/utility');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var response = require('../utilities/response');
var authorization = require('../utilities/authorization');
var _ = require('lodash');
var request = require('request');
var cfg = require('../config/serverCfg.js');
var csv = require('fast-csv');
var fs = require('fs');
var json2csvParser = require('json2csv').Parser;
var csv = require('fast-csv');
//-----------------------------------------------------------------------------------------------------
module.exports = function(app, server) {
    //-------------------------------------------------------------------------------------------------
    var VerifyEmailModel = mongoose.model('VerifyEmail');
    //-------------------------------------------------------------------------------------------------
    //findAll
    app.post('/api/email/check', function (req, res, next) {
        if (!req.connection.remoteAddress.startsWith('167.89')) {
            return response.error(res, 404);
        }
        console.log('------------>');
        console.log();
        console.log('body: ')
        console.log(req.body);

        var arr1 = [{
            ip: '167.89.106.58',
            response: '250 2.0.0 OK 1543406797 l65-v6si4613733ywc.393 - gsmtp ',
            sg_event_id: 'UOr844jQTGOBcZkvpeerTA',
            sg_message_id:
                '27WHG6ZsTFq_PZX3qDbxRQ.filter1850p1mdw1-28436-5BFE84CB-2D.0',
            tls: 1,
            event: 'delivered',
            email: 'marjan.dimov78@gmail.com',
            timestamp: 1543406797,
            'smtp-id': '<32175085-5a52-1667-db9e-d15164464a63@sendgrid.net>'
        }];
       
        var arr2 = [{
            email: 'wr3dsdfsdf.asdffdssd@gmail.com',
            'smtp-id': '<a8fa86e2-82b7-fe6f-0e40-61a6a6944d0c@sendgrid.net>',
            timestamp: 1543406796,
            sg_event_id: 'sTolOGs2T1ivhLEjWpnEzw',
            sg_message_id: 'filter0088p1iad2-20768-5BFE84CC-3.0',
            reason: 'Bounced Address',
            event: 'dropped'
        }];

        if (arr1) {
            if (arr1.length) {
                if (arr1[0].event === 'delivered') {
                    var obj = {
                        email: arr1[0].email,
                        valid: true
                    }
                    VerifyEmailModel.update({ email: arr1[0].email }, obj, { upsert: true, setDefaultsOnInsert: true }, function (err, data) {
                        if (err) {
                            return console.log(err); response.error(res, 500);
                        }
                        return response.ok(res, 200, data);
                    });
                }
            }
        }
        
    });
    app.post('/api/email/verify', function (req, res, next) {
        if (req.files) {
            if (_.indexOf(cfg.allowed_file_types.files, req.files.file.mimetype) === -1) {
                return response.error(res, 500, {}, 'File type is not allowed for upload');
            }
            var path = './resources/files/tmp/' + req.files.file.name;
            req.files.file.mv(path, function (err) {
                if (err) {
                    return response.error(res, 500);
                }
                var headers = [
                    'Firstname',
                    'Lastname',
                    'Domain'
                ];
                var verify = [];
                var parseErrors = [];
                csv.fromPath(path, {
                    headers: headers,
                    ignoreEmpty: true,
                    renameHeaders: true,
                    strictColumnHandling: true
                })
                    .on('data', function (data) {
                        if (parseErrors.length !== 0) {
                            return response.error(res, 500, {}, parseErrors);
                        }
                        verify.push(data);
                    })
                    .on('end', function () {
                        if (parseErrors.length !== 0) {
                            return response.error(res, 500, {}, parseErrors);
                        }
                        if (verify.length === 0) {
                            return response.error(res, 500, {}, 'No Verify Emails data, this file not containing Firstname, Lastname, Domain.');
                        }
                        fs.stat(path, function (err, stats) {
                            if (err) {
                                return console.error(err);
                            }

                            fs.unlink(path, function (err) {
                                if (err) return console.log(err);
                                console.log('file deleted successfully');
                            });
                        });
                        
                        var verificationArr = [];
                        _.each(verify, function (obj) {
                            verificationArr.push({
                                name: obj.Firstname + ' ' + obj.Lastname,
                                domain: obj.Domain
                            });
                        });

                        VerifyEmailModel.bulkWrite(
                            verificationArr.map((data) =>
                                ({
                                    updateOne: {
                                        filter: { firstname: data.Firstname, lastname: data.Lastname },
                                        update: {
                                            $set: {
                                                firstname: data.name.split(' ')[0],
                                                lastname: data.name.split(' ')[1],
                                                domain: data.domain,
                                                email: null,
                                                valid: false
                                            }
                                        },
                                        upsert: true
                                    }
                                })
                            )
                        )
                            .then(bulkWriteOpResult => {
                                request.post(
                                    {
                                        url: 'http://localhost:3050/check_emails_with_domains',
                                        form: { names: verificationArr }
                                    },
                                    function (err, httpResponse, body) {
                                        if (err) {
                                            console.log(err);
                                            return response.error(res, 500);
                                        }
                                        request.post(
                                            {
                                                url: 'http://localhost:3050/check_emails_with_domains',
                                                form: { names: verificationArr }
                                            },
                                            function (err, httpResponse, body) {
                                                if (err) {
                                                    return response.error(res, 500);
                                                }
                                            }
                                        );
                                        return response.ok(res, 200, [], 'Emails for verification successfully imported into database. Emails verification proccess started, this proccess can take a while depending on how many emails is imported.');
                                    }
                                );
                            })
                            .catch(err => {
                                return response.error(res, 500);
                            });
                        
                    });
            });
        }
    });
    app.get('/api/email/export', function (req, res, next) {
        VerifyEmailModel.find({ valid: true }, '-__v')
            .exec(function (err, data) {
                if (err) { return response.error(res, 500); }
                console.log(data);
                try {
                    var fields = [
                        { label: 'Firstname', value: 'firstname' },
                        { label: 'Lastname', value:'lastname' },
                        { label: 'Domain', value: 'domain' },
                        { label: 'Email', value: 'email' },
                        { label: 'Valid', value: 'valid' }
                    ];
                    var opts = { fields };

                    var parser = new json2csvParser(opts);
                    var csv = parser.parse(data);

                    var dtStamp = utility.dateTimeStamp();
                    var path = './resources/files/tmp/_' + dtStamp + '_verified-emails.csv';

                    fs.writeFile(path, csv, 'utf8', function (err) {
                        if (err) {
                            return response.error(res, 500);
                        }
                        else {
                            if (fs.existsSync(path)) {
                                return res.download(path, function (err) {
                                    if (err) { return response.error(res, 500); }
                                    fs.stat(path, function (err, stats) {
                                        if (err) {
                                            return console.error(err);
                                        }

                                        fs.unlink(path, function (err) {
                                            if (err) return console.log(err);
                                            console.log('file deleted successfully');
                                        });
                                    });
                                });
                            }
                            else {
                                response.error(res, 404, 'There are no such file.');
                            }
                        }
                    });
                }
                catch (err) {
                    console.log(err);
                    return response.error(res, 500);
                }
            });
    });
    //-------------------------------------------------------------------------------------------------
};
//-----------------------------------------------------------------------------------------------------