//-----------------------------------------------------------------------------------------------------
var utility = require('../utilities/utility');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var response = require('../utilities/response');
//var cfg = require('../config/serverCfg.js');

var User = mongoose.model('User');

//-----------------------------------------------------------------------------------------------------
var authorization = require('../utilities/authorization.js');
//-----------------------------------------------------------------------------------------------------
module.exports = function(app, server) {
    //-------------------------------------------------------------------------------------------------
    var ObjCtrl = require('../controllers/UserController')(server);
    var ObjModel = mongoose.model('User');
    //-------------------------------------------------------------------------------------------------
    //PUBLIC
    //-------------------------------------------------------------------------------------------------
    app.post('/api/user/register', function (req, res, next) {
        ObjCtrl.register(req, res, next);
    });
    app.post('/api/user/forgotpassword', function (req, res, next) {
        ObjCtrl.forgotpassword(req, res, next);
    });
    app.post('/api/user/login', function(req, res, next) {
        ObjCtrl.login(req, res, next);
    });
    app.post('/api/user/contactus', function (req, res, next) {
        ObjCtrl.contactUs(req, res, next);
    });
    app.get('/api/user/loggeduser', function (req, res, next) {
        ObjCtrl.getLoggedUser(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //SECURED
    //-------------------------------------------------------------------------------------------------
    //findAll
    app.get('/api/user', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.findAll(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //findOne
    app.get('/api/user/:id', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.findOne(req, res, next);
    });
    //findHisOwnAccount
    app.get('/api/user/own/:id', authorization.isAuthorized, function (req, res, next) {
        console.log('route');
        ObjCtrl.findOwnAccount(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //create
    app.post('/api/user',  authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.create(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //destroy
    app.delete('/api/user/:id',  authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.delete(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //update
    app.put('/api/user/:id',  authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.update(req, res, next);
    });
    //updateHisOwnAccount
    app.put('/api/user/own/:id', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.updateOwnAccount(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
};
//-----------------------------------------------------------------------------------------------------