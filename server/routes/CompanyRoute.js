//-----------------------------------------------------------------------------------------------------
var utility = require('../utilities/utility');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var response = require('../utilities/response');
var authorization = require('../utilities/authorization');
//-----------------------------------------------------------------------------------------------------
module.exports = function(app, server) {
    //-------------------------------------------------------------------------------------------------
    var ObjCtrl = require('../controllers/CompanyController')(server);
    //-------------------------------------------------------------------------------------------------
    // PUBLIC
    app.get('/api/company/import', function (req, res, next) {
        ObjCtrl.import(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //findAll
    app.get('/api/company', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.findAll(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //findOne
    app.get('/api/company/:id',  authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.findOne(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //create
    app.post('/api/company',  authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.create(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //destroy
    app.delete('/api/company/:id', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.delete(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //update
    app.put('/api/company/:id',  authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.update(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
};
//-----------------------------------------------------------------------------------------------------