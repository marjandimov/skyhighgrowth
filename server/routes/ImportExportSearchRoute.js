//-----------------------------------------------------------------------------------------------------
var utility = require('../utilities/utility');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var response = require('../utilities/response');
var authorization = require('../utilities/authorization');
//-----------------------------------------------------------------------------------------------------
module.exports = function(app, server) {
    //-------------------------------------------------------------------------------------------------
    var ObjCtrl = require('../controllers/ImportExportSearchController')(server);
    //-------------------------------------------------------------------------------------------------
    // PUBLIC
    app.post('/api/ies/import', function (req, res, next) {
        req.setTimeout(0);
        ObjCtrl.import(req, res, next);
    });
    app.get('/api/ies/filters', function (req, res, next) {
        req.setTimeout(0);
        ObjCtrl.filters(req, res, next);
    });
    //search
    app.post('/api/ies/search', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.search(req, res, next);
    });
    //search
    app.post('/api/ies/export', authorization.isAuthorized, function (req, res, next) {
        req.setTimeout(0);
        ObjCtrl.export(req, res, next);
    });
    app.post('/api/ies/export/selected', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.exportSelected(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
};
//-----------------------------------------------------------------------------------------------------