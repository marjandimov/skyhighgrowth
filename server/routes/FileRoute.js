//-----------------------------------------------------------------------------------------------------
var utility = require('../utilities/utility');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var response = require('../utilities/response');

//-----------------------------------------------------------------------------------------------------
var authorization = require('../utilities/authorization.js');
//-----------------------------------------------------------------------------------------------------
module.exports = function (app, server) {
    //-------------------------------------------------------------------------------------------------
    var ObjCtrl = require('../controllers/FileController')(server);
    //-------------------------------------------------------------------------------------------------
    //findAll
    app.get('/api/file', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.findAll(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------d
    //create
    app.post('/api/file',  authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.create(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //destroy
    app.delete('/api/file/:name',  authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.delete(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
};
//-----------------------------------------------------------------------------------------------------