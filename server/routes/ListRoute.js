//-----------------------------------------------------------------------------------------------------
var utility = require('../utilities/utility');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var response = require('../utilities/response');
var authorization = require('../utilities/authorization');
//-----------------------------------------------------------------------------------------------------
module.exports = function(app, server) {
    //-------------------------------------------------------------------------------------------------
    var ObjCtrl = require('../controllers/ListController')(server);
    //-------------------------------------------------------------------------------------------------

    app.get('/api/list/lists', function (req, res, next) {
        ObjCtrl.getLists(req, res, next);
    });
    app.post('/api/list/downloadsample', function (req, res, next) {
        ObjCtrl.downloadSample(req, res, next);
    });

    //findAll
    app.get('/api/list', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.findAll(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //findOne
    app.get('/api/list/:id',  authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.findOne(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //create
    app.post('/api/list',  authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.create(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //destroy
    app.delete('/api/list/:id', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.delete(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //update
    app.put('/api/list/:id',  authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.update(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    app.post('/api/list/file', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.upload(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
};
//-----------------------------------------------------------------------------------------------------