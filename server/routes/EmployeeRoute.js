//-----------------------------------------------------------------------------------------------------
var utility = require('../utilities/utility');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var response = require('../utilities/response');
var authorization = require('../utilities/authorization');
//-----------------------------------------------------------------------------------------------------
module.exports = function(app, server) {
    //-------------------------------------------------------------------------------------------------
    var ObjCtrl = require('../controllers/EmployeeController')(server);
    //-------------------------------------------------------------------------------------------------
    // PUBLIC
    app.get('/api/employee/import', function (req, res, next) {
        ObjCtrl.import(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //findAll
    app.get('/api/employee', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.findAll(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //findOne
    app.get('/api/employee/:id',  authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.findOne(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //create
    app.post('/api/employee',  authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.create(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //destroy
    app.delete('/api/employee/:id', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.delete(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
    //update
    app.put('/api/employee/:id',  authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.update(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
};
//-----------------------------------------------------------------------------------------------------