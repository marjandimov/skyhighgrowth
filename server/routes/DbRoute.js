//-----------------------------------------------------------------------------------------------------
var utility = require('../utilities/utility');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var response = require('../utilities/response');
var authorization = require('../utilities/authorization');
//-----------------------------------------------------------------------------------------------------
module.exports = function(app, server) {
    //-------------------------------------------------------------------------------------------------
    var ObjCtrl = require('../controllers/DbController')(server);
    //-------------------------------------------------------------------------------------------------
    // PUBLIC
    app.get('/api/db/filters', function (req, res, next) {
        req.setTimeout(0);
        ObjCtrl.filters(req, res, next);
    });
    //search
    app.post('/api/db/search', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.search(req, res, next);
    });
    //search
    app.post('/api/db/export/selected', authorization.isAuthorized, function (req, res, next) {
        ObjCtrl.exportSelected(req, res, next);
    });
    //-------------------------------------------------------------------------------------------------
};
//-----------------------------------------------------------------------------------------------------