module.exports = {
    production: false,
    fill_db_on_init: false,
    locale: { en: true, sr: true, de: true, fr: true, ru: true, nl: true }, //not implemented
    connection_limit: 0, //not implemented
    disable_registraion: true,
    log_to_console: true,
    production_settings: {
        ip: 'localhost',
        port: 8000,
        mongodb_connection: 'mongodb://localhost:27017/shg',
        cors_origin: 'localhost',
        redis_origin: '',
        redis_host: 'localhost',
        redis_port: 6379,
        redis_password: 'redis1234!@#$',
        redis_expiration_hours: 5,
        home_dir: '/Users/marjan/Projects/SkyHighGrowth',
        mail: {
            user: 'skyhighgrowthdb@gmail.com',
            pass: 'shg1234!'
        }
    },
    development_settings: {
        ip: 'localhost',
        port: 8000,
        mongodb_connection: 'mongodb://localhost:27017/shg',
        cors_origin: 'localhost',
        redis_origin: '',
        redis_host: 'localhost',
        redis_port: 6379,
        redis_password: 'redis1234!@#$',
        redis_expiration_hours: 5,
        home_dir: '/Users/marjan/Projects/SkyHighGrowth',
        mail: {
            user: 'skyhighgrowthdb@gmail.com',
            pass: 'shg1234!'
        }
    },
    token_expiration: '12h', // in hours
    secret_key: 'XeT!a#&QiOkW$q3%aOqJ5l6-uR8xB^)G',
    encrypt_key: 'Aq5^aOq$5l@WeS(R',
    cookie_key: 'e32rW5i8(_oHb23#',
    salt_factor: 10,
    account_lock_time: 2 * 60 * 60 * 1000, //2h
    max_login_attempts: 5,
    allowed_file_types: {
        images: [],
        files: ['application/vnd.ms-excel', 'text/csv']
    },
    max_upload_size: 5 * 1024 * 1024 // 5mb
};